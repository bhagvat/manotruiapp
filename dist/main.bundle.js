webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/addnew/addnew.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/addnew/addnew.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  addnew works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/addnew/addnew.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddnewComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AddnewComponent = (function () {
    function AddnewComponent() {
    }
    AddnewComponent.prototype.ngOnInit = function () {
    };
    AddnewComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-addnew',
            template: __webpack_require__("../../../../../src/app/addnew/addnew.component.html"),
            styles: [__webpack_require__("../../../../../src/app/addnew/addnew.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AddnewComponent);
    return AddnewComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "li:active {\n    color: red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\" class=\"container\">\n    <div id=\"menu\">\n        <div id=\"primary-menu\" class=\"primary-menu\">\n            <div class=\"row\">\n                <div class=\"logo col-md-3\">\n                    <a href=\"/\"> <img src=\"./assets/images/manotr-logo.png\"></a>\n                </div>\n                <!-- logo -->\n                <div id=\"search\" class=\"search col-md-4\">\n                    <input type=\"text\" name=\"search\" placeholder=\"SEARCH NEWS\">\n                    <div class=\"search-icon\">\n                        <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- search -->\n                <div class=\"news-switcher col-md-2\">\n                    <div class=\"read-mode-on\" routerLink=\"/\"></div>\n                    <div class=\"watch-mode\" routerLink=\"/politics-video\"></div>\n                </div>\n                <!-- news-switcher -->\n                <div class=\"login col-md-2\">\n                    <i class=\"fa fa-user-o\" aria-hidden=\"true\"></i>\n                    <a routerLink=\"/signin-signup\">SIGN IN / SIGN UP</a>\n                </div>\n                <!-- login -->\n            </div>\n            <!-- row -->\n        </div>\n        <!-- primary-menu -->\n        <div id=\"secondary-menu\" class=\"secondary-menu\">\n            <nav class=\"navbar navbar-default\" role=\"navigation\">\n                <div class=\"container-fluid\">\n                    <!-- Brand and toggle get grouped for better mobile display -->\n                    <div class=\"navbar-header\">\n                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\">\n\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>\n\t\t\t\t\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t\t\t\t</button>\n                    </div>\n\n                    <!-- Collect the nav links, forms, and other content for toggling -->\n                    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">\n                        <ul class=\"nav navbar-nav\">\n                            <li class=\"active\"><a href=\"#\">Politics</a></li>\n                            <li><a routerLink=\"/worldnews\" routerLinkActive=\"active\">World News</a></li>\n                            <li><a routerLink=\"/cricket\" routerLinkActive=\"active\">Cricket</a></li>\n                            <li><a routerLink=\"/technology\" routerLinkActive=\"active\">Technology</a></li>\n                            <li><a routerLink=\"/startup\" routerLinkActive=\"active\">Startups</a></li>\n                            <li><a routerLink=\"/gaming\" routerLinkActive=\"active\">Gaming</a></li>\n                            <li><a routerLink=\"/tvshows\" routerLinkActive=\"active\">TV Shows</a></li>\n                            <li><a routerLink=\"/bollywood\" routerLinkActive=\"active\">Bollywood</a></li>\n                            <li><a routerLink=\"/finance\" routerLinkActive=\"active\">Finance</a></li>\n                            <li><a routerLink=\"/videos\" routerLinkActive=\"active\">Videos</a></li>\n                            <ul class=\"nav navbar-nav navbar-right\">\n                                <li class=\"dropdown\">\n                                    <a href=\"#\" class=\"last-item dropdown-toggle\" data-toggle=\"dropdown\">Add New</a>\n                                    <ul class=\"dropdown-menu\">\n                                        <li><a href=\"#\">Action</a></li>\n                                        <li><a href=\"#\">Another action</a></li>\n                                        <li><a href=\"#\">Something else here</a></li>\n                                        <li><a href=\"#\">Separated link</a></li>\n                                    </ul>\n                                </li>\n                            </ul>\n                        </ul>\n                    </div>\n                    <!-- /.navbar-collapse -->\n                </div>\n            </nav>\n        </div>\n        <!-- secondary-menu -->\n    </div>\n\n    <router-outlet></router-outlet>\n\n</div>\n\n<footer>\n    <div class=\"footer clearfix\">\n        <div class=\"col-md-2 gray\">\n            <img src=\"./assets/images/manotr-logo.png\">\n        </div>\n        <div class=\"col-md-8\">\n            <p>Follow Us: </p><i class=\"fa fa-google-plus\" aria-hidden=\"true\"></i>&nbsp;<i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>&nbsp;<i class=\"fa fa-facebook\" aria-hidden=\"true\"></i>&nbsp;<i class=\"fa fa-vk\" aria-hidden=\"true\"></i>&nbsp;<i class=\"fa fa-youtube-play\"\n                aria-hidden=\"true\"></i>\n        </div>\n        <div class=\"col-md-2\">\n            <p>2017 &copy; All Rights Reserved</p>\n        </div>\n    </div>\n    <!-- footer -->\n</footer>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());

$(function () {
    $('.nav a').filter(function () {
        return this.href == location.href;
    }).parent().addClass('active').siblings().removeClass('active');
    $('.nav a').click(function () {
        $(this).parent().addClass('active').siblings().removeClass('active');
    });
});


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_carouselamos__ = __webpack_require__("../../../../ng2-carouselamos/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_infinite_scroll__ = __webpack_require__("../../../../ngx-infinite-scroll/modules/ngx-infinite-scroll.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__main_headlines_main_headlines_component__ = __webpack_require__("../../../../../src/app/main-headlines/main-headlines.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__news_desk_news_desk_component__ = __webpack_require__("../../../../../src/app/news-desk/news-desk.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__scandal_watch_scandal_watch_component__ = __webpack_require__("../../../../../src/app/scandal-watch/scandal-watch.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__think_tank_think_tank_component__ = __webpack_require__("../../../../../src/app/think-tank/think-tank.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__around_world_around_world_component__ = __webpack_require__("../../../../../src/app/around-world/around-world.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__world_news_world_news_component__ = __webpack_require__("../../../../../src/app/world-news/world-news.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__cricket_cricket_component__ = __webpack_require__("../../../../../src/app/cricket/cricket.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__technology_technology_component__ = __webpack_require__("../../../../../src/app/technology/technology.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__startups_startups_component__ = __webpack_require__("../../../../../src/app/startups/startups.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__gaming_gaming_component__ = __webpack_require__("../../../../../src/app/gaming/gaming.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__tv_shows_tv_shows_component__ = __webpack_require__("../../../../../src/app/tv-shows/tv-shows.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__bollywood_bollywood_component__ = __webpack_require__("../../../../../src/app/bollywood/bollywood.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__finance_finance_component__ = __webpack_require__("../../../../../src/app/finance/finance.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__addnew_addnew_component__ = __webpack_require__("../../../../../src/app/addnew/addnew.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__politics_politics_component__ = __webpack_require__("../../../../../src/app/politics/politics.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__videos_videos_component__ = __webpack_require__("../../../../../src/app/videos/videos.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__scandalwatch_viewdetail_scandalwatch_viewdetail_component__ = __webpack_require__("../../../../../src/app/scandalwatch-viewdetail/scandalwatch-viewdetail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__newsdesk_viewdetail_newsdesk_viewdetail_component__ = __webpack_require__("../../../../../src/app/newsdesk-viewdetail/newsdesk-viewdetail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__aroundworld_viewdetail_aroundworld_viewdetail_component__ = __webpack_require__("../../../../../src/app/aroundworld-viewdetail/aroundworld-viewdetail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__login_signup_login_signup_component__ = __webpack_require__("../../../../../src/app/login-signup/login-signup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__signin_signup_signin_signup_component__ = __webpack_require__("../../../../../src/app/signin-signup/signin-signup.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__news_landing_news_landing_component__ = __webpack_require__("../../../../../src/app/news-landing/news-landing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__news_list_news_list_component__ = __webpack_require__("../../../../../src/app/news-list/news-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__category_listing_category_listing_component__ = __webpack_require__("../../../../../src/app/category-listing/category-listing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__user_dashboard_user_dashboard_component__ = __webpack_require__("../../../../../src/app/user-dashboard/user-dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__politics_video_politics_video_component__ = __webpack_require__("../../../../../src/app/politics-video/politics-video.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__coroseldemo_coroseldemo_component__ = __webpack_require__("../../../../../src/app/coroseldemo/coroseldemo.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__jquerycorosel_jquerycorosel_component__ = __webpack_require__("../../../../../src/app/jquerycorosel/jquerycorosel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__simpleslider_simpleslider_component__ = __webpack_require__("../../../../../src/app/simpleslider/simpleslider.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__news_landing_news_details_news_landing_news_details_component__ = __webpack_require__("../../../../../src/app/news-landing-news-details/news-landing-news-details.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__news_landing_related_news_rightt_div_news_landing_related_news_rightt_div_component__ = __webpack_require__("../../../../../src/app/news-landing-related-news-rightt-div/news-landing-related-news-rightt-div.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__news_landing_graph_news_landing_graph_component__ = __webpack_require__("../../../../../src/app/news-landing-graph/news-landing-graph.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__news_landing_related_news_bottom_div_news_landing_related_news_bottom_div_component__ = __webpack_require__("../../../../../src/app/news-landing-related-news-bottom-div/news-landing-related-news-bottom-div.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__news_landing_tags_news_landing_tags_component__ = __webpack_require__("../../../../../src/app/news-landing-tags/news-landing-tags.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__main_headlines_service__ = __webpack_require__("../../../../../src/app/main-headlines.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__scandal_watch_service__ = __webpack_require__("../../../../../src/app/scandal-watch.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__think_tank_service__ = __webpack_require__("../../../../../src/app/think-tank.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__around_world_service__ = __webpack_require__("../../../../../src/app/around-world.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__news_desk_service__ = __webpack_require__("../../../../../src/app/news-desk.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__news_landing_related_news_right_service__ = __webpack_require__("../../../../../src/app/news-landing-related-news-right.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__newslanding_news_details_service__ = __webpack_require__("../../../../../src/app/newslanding-news-details.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__newslanding_bottomdivnews_service__ = __webpack_require__("../../../../../src/app/newslanding-bottomdivnews.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__scrolldemo_scrolldemo_component__ = __webpack_require__("../../../../../src/app/scrolldemo/scrolldemo.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__viewmore_service__ = __webpack_require__("../../../../../src/app/viewmore.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__category_main_category_main_component__ = __webpack_require__("../../../../../src/app/category-main/category-main.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/* import requird global module for all components here */






/* import all components here */



































/* import all services here */











/* add routing here */
var appRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_21__politics_politics_component__["a" /* PoliticsComponent */],
    },
    {
        path: 'worldnews',
        component: __WEBPACK_IMPORTED_MODULE_12__world_news_world_news_component__["a" /* WorldNewsComponent */],
    },
    {
        path: 'cricket',
        component: __WEBPACK_IMPORTED_MODULE_13__cricket_cricket_component__["a" /* CricketComponent */],
    },
    {
        path: 'technology',
        component: __WEBPACK_IMPORTED_MODULE_14__technology_technology_component__["a" /* TechnologyComponent */],
    },
    {
        path: 'startup',
        component: __WEBPACK_IMPORTED_MODULE_15__startups_startups_component__["a" /* StartupsComponent */],
    },
    {
        path: 'gaming',
        component: __WEBPACK_IMPORTED_MODULE_16__gaming_gaming_component__["a" /* GamingComponent */],
    },
    {
        path: 'tvshows',
        component: __WEBPACK_IMPORTED_MODULE_17__tv_shows_tv_shows_component__["a" /* TvShowsComponent */],
    },
    {
        path: 'bollywood',
        component: __WEBPACK_IMPORTED_MODULE_18__bollywood_bollywood_component__["a" /* BollywoodComponent */],
    },
    {
        path: 'finance',
        component: __WEBPACK_IMPORTED_MODULE_19__finance_finance_component__["a" /* FinanceComponent */],
    },
    {
        path: 'videos',
        component: __WEBPACK_IMPORTED_MODULE_22__videos_videos_component__["a" /* VideosComponent */],
    },
    {
        path: 'aroundworld-details',
        component: __WEBPACK_IMPORTED_MODULE_25__aroundworld_viewdetail_aroundworld_viewdetail_component__["a" /* AroundworldViewdetailComponent */]
    },
    {
        path: 'newsdesk-details',
        component: __WEBPACK_IMPORTED_MODULE_24__newsdesk_viewdetail_newsdesk_viewdetail_component__["a" /* NewsdeskViewdetailComponent */]
    },
    {
        path: 'scandalwatch-details',
        component: __WEBPACK_IMPORTED_MODULE_23__scandalwatch_viewdetail_scandalwatch_viewdetail_component__["a" /* ScandalwatchViewdetailComponent */]
    },
    {
        path: 'signin-signup',
        component: __WEBPACK_IMPORTED_MODULE_27__signin_signup_signin_signup_component__["a" /* SigninSignupComponent */]
    },
    {
        path: 'news-landing',
        component: __WEBPACK_IMPORTED_MODULE_28__news_landing_news_landing_component__["a" /* NewsLandingComponent */]
    },
    {
        path: 'news-list',
        component: __WEBPACK_IMPORTED_MODULE_29__news_list_news_list_component__["a" /* NewsListComponent */]
    },
    {
        path: 'category-listing',
        component: __WEBPACK_IMPORTED_MODULE_30__category_listing_category_listing_component__["a" /* CategoryListingComponent */]
    },
    {
        path: 'user-dahboard',
        component: __WEBPACK_IMPORTED_MODULE_31__user_dashboard_user_dashboard_component__["a" /* UserDashboardComponent */]
    },
    {
        path: 'politics-video',
        component: __WEBPACK_IMPORTED_MODULE_32__politics_video_politics_video_component__["a" /* PoliticsVideoComponent */]
    },
    {
        path: 'corosel',
        component: __WEBPACK_IMPORTED_MODULE_33__coroseldemo_coroseldemo_component__["a" /* CoroseldemoComponent */]
    },
    {
        path: 'jqcorosel',
        component: __WEBPACK_IMPORTED_MODULE_34__jquerycorosel_jquerycorosel_component__["a" /* JquerycoroselComponent */]
    },
    {
        path: 'simpleslider',
        component: __WEBPACK_IMPORTED_MODULE_35__simpleslider_simpleslider_component__["a" /* SimplesliderComponent */]
    },
    {
        path: 'scrolldemo',
        component: __WEBPACK_IMPORTED_MODULE_49__scrolldemo_scrolldemo_component__["a" /* ScrolldemoComponent */]
    },
    {
        path: 'maincategory',
        component: __WEBPACK_IMPORTED_MODULE_51__category_main_category_main_component__["a" /* CategoryMainComponent */]
    }
];
/* add all compoenets reference here */
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__main_headlines_main_headlines_component__["a" /* MainHeadlinesComponent */],
                __WEBPACK_IMPORTED_MODULE_8__news_desk_news_desk_component__["a" /* NewsDeskComponent */],
                __WEBPACK_IMPORTED_MODULE_9__scandal_watch_scandal_watch_component__["a" /* ScandalWatchComponent */],
                __WEBPACK_IMPORTED_MODULE_10__think_tank_think_tank_component__["a" /* ThinkTankComponent */],
                __WEBPACK_IMPORTED_MODULE_11__around_world_around_world_component__["a" /* AroundWorldComponent */],
                __WEBPACK_IMPORTED_MODULE_12__world_news_world_news_component__["a" /* WorldNewsComponent */],
                __WEBPACK_IMPORTED_MODULE_13__cricket_cricket_component__["a" /* CricketComponent */],
                __WEBPACK_IMPORTED_MODULE_14__technology_technology_component__["a" /* TechnologyComponent */],
                __WEBPACK_IMPORTED_MODULE_15__startups_startups_component__["a" /* StartupsComponent */],
                __WEBPACK_IMPORTED_MODULE_16__gaming_gaming_component__["a" /* GamingComponent */],
                __WEBPACK_IMPORTED_MODULE_17__tv_shows_tv_shows_component__["a" /* TvShowsComponent */],
                __WEBPACK_IMPORTED_MODULE_18__bollywood_bollywood_component__["a" /* BollywoodComponent */],
                __WEBPACK_IMPORTED_MODULE_19__finance_finance_component__["a" /* FinanceComponent */],
                __WEBPACK_IMPORTED_MODULE_20__addnew_addnew_component__["a" /* AddnewComponent */],
                __WEBPACK_IMPORTED_MODULE_21__politics_politics_component__["a" /* PoliticsComponent */],
                __WEBPACK_IMPORTED_MODULE_22__videos_videos_component__["a" /* VideosComponent */],
                __WEBPACK_IMPORTED_MODULE_23__scandalwatch_viewdetail_scandalwatch_viewdetail_component__["a" /* ScandalwatchViewdetailComponent */],
                __WEBPACK_IMPORTED_MODULE_24__newsdesk_viewdetail_newsdesk_viewdetail_component__["a" /* NewsdeskViewdetailComponent */],
                __WEBPACK_IMPORTED_MODULE_25__aroundworld_viewdetail_aroundworld_viewdetail_component__["a" /* AroundworldViewdetailComponent */],
                __WEBPACK_IMPORTED_MODULE_26__login_signup_login_signup_component__["a" /* LoginSignupComponent */],
                __WEBPACK_IMPORTED_MODULE_27__signin_signup_signin_signup_component__["a" /* SigninSignupComponent */],
                __WEBPACK_IMPORTED_MODULE_28__news_landing_news_landing_component__["a" /* NewsLandingComponent */],
                __WEBPACK_IMPORTED_MODULE_29__news_list_news_list_component__["a" /* NewsListComponent */],
                __WEBPACK_IMPORTED_MODULE_30__category_listing_category_listing_component__["a" /* CategoryListingComponent */],
                __WEBPACK_IMPORTED_MODULE_31__user_dashboard_user_dashboard_component__["a" /* UserDashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_32__politics_video_politics_video_component__["a" /* PoliticsVideoComponent */],
                __WEBPACK_IMPORTED_MODULE_33__coroseldemo_coroseldemo_component__["a" /* CoroseldemoComponent */],
                __WEBPACK_IMPORTED_MODULE_34__jquerycorosel_jquerycorosel_component__["a" /* JquerycoroselComponent */],
                __WEBPACK_IMPORTED_MODULE_35__simpleslider_simpleslider_component__["a" /* SimplesliderComponent */],
                __WEBPACK_IMPORTED_MODULE_36__news_landing_news_details_news_landing_news_details_component__["a" /* NewsLandingNewsDetailsComponent */],
                __WEBPACK_IMPORTED_MODULE_37__news_landing_related_news_rightt_div_news_landing_related_news_rightt_div_component__["a" /* NewsLandingRelatedNewsRighttDivComponent */],
                __WEBPACK_IMPORTED_MODULE_38__news_landing_graph_news_landing_graph_component__["a" /* NewsLandingGraphComponent */],
                __WEBPACK_IMPORTED_MODULE_39__news_landing_related_news_bottom_div_news_landing_related_news_bottom_div_component__["a" /* NewsLandingRelatedNewsBottomDivComponent */],
                __WEBPACK_IMPORTED_MODULE_40__news_landing_tags_news_landing_tags_component__["a" /* NewsLandingTagsComponent */],
                __WEBPACK_IMPORTED_MODULE_49__scrolldemo_scrolldemo_component__["a" /* ScrolldemoComponent */],
                __WEBPACK_IMPORTED_MODULE_51__category_main_category_main_component__["a" /* CategoryMainComponent */]
            ],
            /*add all import module refeence here */
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4_ngx_infinite_scroll__["a" /* InfiniteScrollModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_carouselamos__["a" /* Ng2CarouselamosModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */].forRoot(appRoutes)
            ],
            /* add all of yours services reference here inside providers array */
            providers: [__WEBPACK_IMPORTED_MODULE_50__viewmore_service__["a" /* ViewmoreService */], __WEBPACK_IMPORTED_MODULE_48__newslanding_bottomdivnews_service__["a" /* NewslandingBottomdivnewsService */], __WEBPACK_IMPORTED_MODULE_41__main_headlines_service__["a" /* MainHeadlinesService */], __WEBPACK_IMPORTED_MODULE_46__news_landing_related_news_right_service__["a" /* NewsLandingRelatedNewsRightService */], __WEBPACK_IMPORTED_MODULE_47__newslanding_news_details_service__["a" /* NewslandingNewsDetailsService */], __WEBPACK_IMPORTED_MODULE_42__scandal_watch_service__["a" /* ScandalWatchService */], __WEBPACK_IMPORTED_MODULE_43__think_tank_service__["a" /* ThinkTankService */], __WEBPACK_IMPORTED_MODULE_44__around_world_service__["a" /* AroundWorldService */], __WEBPACK_IMPORTED_MODULE_45__news_desk_service__["a" /* NewsDeskService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());

Object(__WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(AppModule);


/***/ }),

/***/ "../../../../../src/app/around-world.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AroundWorldService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AroundWorldService = (function () {
    function AroundWorldService() {
        this.aroundWorldNews = [
            {
                image: "http://www.lorempixel.com/270/180",
                source: "",
                category: "",
                occurance: "October 6, 2017",
                views: "1465",
                title: "This is a news title",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            },
            {
                image: "http://www.lorempixel.com/270/180",
                source: "",
                category: "",
                occurance: "October 6, 2017",
                views: "1465",
                title: "This is a news title",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            },
            {
                image: "http://www.lorempixel.com/270/180",
                source: "",
                category: "",
                occurance: "October 6, 2017",
                views: "1465",
                title: "This is a news title",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            },
            {
                image: "http://www.lorempixel.com/270/180",
                source: "",
                category: "",
                occurance: "October 6, 2017",
                views: "1465",
                title: "This is a news title",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
            },
        ];
    }
    AroundWorldService.prototype.getAroundWordNews = function () {
        console.log("in get around world news");
        return this.aroundWorldNews;
    };
    AroundWorldService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], AroundWorldService);
    return AroundWorldService;
}());



/***/ }),

/***/ "../../../../../src/app/around-world/around-world.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".padding-0 {\n    padding-right: 0;\n    padding-left: 0;\n}\n\n.news1 {\n    cursor: pointer;\n}\n\n.nextbtn:hover {\n    box-shadow: 0px 0px 150px #000000;\n    z-index: 2;\n    -webkit-transition: all 200ms ease-in;\n    -webkit-transform: scale(1.5);\n    -ms-transition: all 200ms ease-in;\n    -ms-transform: scale(1.5);\n    -moz-transition: all 200ms ease-in;\n    -moz-transform: scale(1.5);\n    transition: all 200ms ease-in;\n    transform: scale(1.5);\n    cursor: pointer\n}\n\n.preButton:hover {\n    box-shadow: 0px 0px 150px #000000;\n    z-index: 2;\n    -webkit-transition: all 200ms ease-in;\n    -webkit-transform: scale(1.5);\n    -ms-transition: all 200ms ease-in;\n    -ms-transform: scale(1.5);\n    -moz-transition: all 200ms ease-in;\n    -moz-transform: scale(1.5);\n    transition: all 200ms ease-in;\n    transform: scale(1.5);\n    cursor: pointer\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/around-world/around-world.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"around-the-world\">\n    <h3>Around the world</h3>\n    <div class=\"underline\"></div>\n    <button class=\"view-all\" routerLink=\"/category-listing\">  view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n    <div class=\"atw-wrapper\">\n\n\n        <div class=\"nextbtn\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"slide 1 of 4\">\n            <button><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></button>\n        </div>\n        <div class=\"preButton\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"slide 1 of 4\">\n            <button><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></button>\n        </div>\n\n\n        <div class=\"news1\" *ngFor=\"let item of data; let i = index\" routerLink=\"/news-landing\">\n            <div class=\"grid-one\">\n                <img src={{item.image}}>\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>{{item.title}}</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">{{item.occurance}}}</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{item.views}}}</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>{{item.description}}</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n\n\n\n    </div>\n    <!-- atw-wrapper -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/around-world/around-world.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AroundWorldComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__around_world_service__ = __webpack_require__("../../../../../src/app/around-world.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AroundWorldComponent = (function () {
    function AroundWorldComponent(AroundWorldService) {
        this.AroundWorldService = AroundWorldService;
        this.data = '';
        this.items = '';
    }
    AroundWorldComponent.prototype.ngOnInit = function () {
        this.data = this.AroundWorldService.getAroundWordNews();
        console.log(this.data);
    };
    AroundWorldComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-around-world',
            template: __webpack_require__("../../../../../src/app/around-world/around-world.component.html"),
            styles: [__webpack_require__("../../../../../src/app/around-world/around-world.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__around_world_service__["a" /* AroundWorldService */]])
    ], AroundWorldComponent);
    return AroundWorldComponent;
}());



/***/ }),

/***/ "../../../../../src/app/aroundworld-viewdetail/aroundworld-viewdetail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/aroundworld-viewdetail/aroundworld-viewdetail.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/aroundworld-viewdetail/aroundworld-viewdetail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AroundworldViewdetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AroundworldViewdetailComponent = (function () {
    function AroundworldViewdetailComponent() {
    }
    AroundworldViewdetailComponent.prototype.ngOnInit = function () {
    };
    AroundworldViewdetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-aroundworld-viewdetail',
            template: __webpack_require__("../../../../../src/app/aroundworld-viewdetail/aroundworld-viewdetail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/aroundworld-viewdetail/aroundworld-viewdetail.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AroundworldViewdetailComponent);
    return AroundworldViewdetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/bollywood/bollywood.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/bollywood/bollywood.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/bollywood/bollywood.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BollywoodComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BollywoodComponent = (function () {
    function BollywoodComponent() {
    }
    BollywoodComponent.prototype.ngOnInit = function () {
    };
    BollywoodComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-bollywood',
            template: __webpack_require__("../../../../../src/app/bollywood/bollywood.component.html"),
            styles: [__webpack_require__("../../../../../src/app/bollywood/bollywood.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BollywoodComponent);
    return BollywoodComponent;
}());



/***/ }),

/***/ "../../../../../src/app/category-listing/category-listing.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".news1:hover {\n    cursor: pointer;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/category-listing/category-listing.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container main-grid-wrapper clearfix\">\n    <div class=\"main-grid\">\n        <h2>More From Politics</h2>\n        <div class=\"search-results\" infiniteScroll [infiniteScrollDistance]=\"2\" [infiniteScrollThrottle]=\"1000\" (scrolled)=\"onScroll()\" [scrollWindow]=\"true\">\n\n            <div class=\"news1\" *ngFor=\"let item of datas; let i=index\" routerLink=\"/news-landing\">\n                <div class=\"grid-one\">\n                    <img src=\"{{item.image}}\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <h3>{{item.title}} {{item.cardNo}}</h3>\n\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">{{item.occurance}}</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{item.views}}</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>{{item.description}}</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n        </div>\n        <!-- news1 -->\n\n        <div class=\"loading-symbol\" align=\"center\" *ngIf=\"count<res.length?true:false\">\n            <img src=\"../../assets/images/805.gif\" style=\"height:25px;width:25px\">\n        </div>\n    </div>\n    <!-- main-grid -->\n    <div class=\"side-grid\">\n        <h4>Related News From Politics</h4>\n        <div class=\"underline\"></div>\n        <div class=\"news1\" routerLink=\"/news-landing\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/100x80\">\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n                <div class=\"occurance\">\n                    <p class=\"grey\" href=\"\">October 6, 2017</p>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <div class=\"news1\" routerLink=\"/news-landing\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/100x80\">\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n                <div class=\"occurance\">\n                    <p class=\"grey\" href=\"\">October 6, 2017</p>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <div class=\"news1\" routerLink=\"/news-landing\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/100x80\">\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n                <div class=\"occurance\">\n                    <p class=\"grey\" href=\"\">October 6, 2017</p>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <div class=\"news1\" routerLink=\"/news-landing\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/100x80\">\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n                <div class=\"occurance\">\n                    <p class=\"grey\" href=\"\">October 6, 2017</p>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <div class=\"news1\" routerLink=\"/news-landing\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/100x80\">\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n                <div class=\"occurance\">\n                    <p class=\"grey\" href=\"\">October 6, 2017</p>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <!-- <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button> -->\n    </div>\n    <!-- side-grid -->\n\n    <div class=\"main-panel\">\n        <div infiniteScroll [infiniteScrollDistance]=\"2\" [infiniteScrollThrottle]=\"50\" [infiniteScrollContainer]=\"selector\" [fromRoot]=\"true\" (scrolled)=\"onScroll()\">\n        </div>\n    </div>"

/***/ }),

/***/ "../../../../../src/app/category-listing/category-listing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryListingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__viewmore_service__ = __webpack_require__("../../../../../src/app/viewmore.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CategoryListingComponent = (function () {
    function CategoryListingComponent(viewmore) {
        this.viewmore = viewmore;
        this.data = '';
        this.posts = '';
        this.i = '';
        this.myArray = '';
        this.chunk_size = '';
        this.result = '';
        this.datas = [];
        this.res = '';
        this.count = 0;
    }
    CategoryListingComponent.prototype.ngOnInit = function () {
        this.data = this.viewmore.getmoreNews();
        // myArray = this.data;
        // chunk_size = 10;
        this.res = this.chunkArray(this.data, 2);
        for (var i = 0; i < this.res[this.count].length; i++) {
            this.res[this.count][i].cardNo = this.count + '-' + i;
            this.datas.push(this.res[this.count][i]);
        }
        this.count++;
    };
    CategoryListingComponent.prototype.chunkArray = function (myArray, chunk_size) {
        var index = 0;
        var arrayLength = myArray.length;
        var tempArray = [];
        for (index = 0; index < arrayLength; index += chunk_size) {
            var myChunk = myArray.slice(index, index + chunk_size);
            // Do something if you want with the group
            tempArray.push(myChunk);
        }
        console.log(tempArray);
        return tempArray;
    };
    CategoryListingComponent.prototype.onScroll = function () {
        var len = this.res.length;
        if (this.count < len) {
            for (var i = 0; i < this.res[this.count].length; i++) {
                this.res[this.count][i].cardNo = this.count + '-' + i;
                this.datas.push(this.res[this.count][i]);
            }
            this.count++;
        }
        // this.datas.push(this.res[this.count])
        //this.datas=this.res[this.count]
        console.log('scrolled!!');
    };
    CategoryListingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-category-listing',
            template: __webpack_require__("../../../../../src/app/category-listing/category-listing.component.html"),
            styles: [__webpack_require__("../../../../../src/app/category-listing/category-listing.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__viewmore_service__["a" /* ViewmoreService */]])
    ], CategoryListingComponent);
    return CategoryListingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/category-main/category-main.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/category-main/category-main.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"categoryBackground\"></div>\n<div id=\"section-two\" class=\"container\">\n    <div class=\"topic-name\">\n        <h2>Politics</h2>\n    </div>\n    <div id=\"scandal-watch\">\n        <h2>HeadLines</h2>\n        <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n        <div class=\"underline\"></div>\n        <div class=\"grid-one-big\">\n            <!-- <div class=\"banner-image\">\n\t\t\t\t\t\t\t<img src=\"http://placehold.it/550x225\">\n\t\t\t\t\t\t\t<div class=\"banner-overlay\"></div>\n\t\t\t\t\t\t</div> -->\n            <div class=\"slider\">\n                <div><img src=\"http://placehold.it/550x225\"></div>\n                <div><img src=\"http://placehold.it/550x225\"></div>\n                <div><img src=\"http://placehold.it/550x225\"></div>\n                <div><img src=\"http://placehold.it/550x225\"></div>\n            </div>\n        </div>\n        <!-- grid-one-big -->\n        <h3>THINK TANK</h3>\n        <div class=\"underline\"></div>\n        <div class=\"think-tank\">\n            <div class=\"grid-one\">\n                <div class=\"banner-image\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"lower-buttons\">\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"banner-contents\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <div class=\"description\">\n                        <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many\n                            presidential campaigns. Some candidates survived. Other didn't</p>\n                    </div>\n                    <div class=\"occurance\">\n                        <p class=\"grey\" href=\"\">October 6, 2017</p>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-one\">\n                <div class=\"banner-image\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"lower-buttons\">\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"banner-contents\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <div class=\"description\">\n                        <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many\n                            presidential campaigns. Some candidates survived. Other didn't</p>\n                    </div>\n                    <div class=\"occurance\">\n                        <p class=\"grey\" href=\"\">October 6, 2017</p>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n        </div>\n    </div>\n    <!-- scandal-watch -->\n    <div id=\"news-desk\">\n        <h2>NEWS DESK</h2>\n        <div class=\"underline\"></div>\n        <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n        <div class=\"news1\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <hr>\n        <div class=\"news2\">\n            <div class=\"grid-one\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <div class=\"grid-two\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news2 -->\n        <hr>\n        <div class=\"news1\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n    </div>\n    <!-- news-desk -->\n</div>\n<!-- section-two -->\n<div class=\"world-news\">\n    <div class=\"container\">\n        <h2>World news</h2>\n        <div class=\"underline\"></div>\n        <div class=\"grid-one-big\">\n            <div class=\"banner-image\">\n                <img src=\"http://placehold.it/1140x466\">\n                <div class=\"banner-overlay\"></div>\n            </div>\n            <div class=\"description\">\n                <h3>\"</h3>\n                <h3>HISTORIC PRESIDENTIAL CAMPAIGN Scandal</h3>\n                <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many presidential\n                    campaigns. Some candidates survived. Other didn't</p>\n                <div class=\"sources\">\n                    <button id=\"source1\" class=\"active\"> Source 1</button>\n                    <button id=\"source2\"> Source 1</button>\n                    <button id=\"source3\"> Source 1</button>\n                    <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                </div>\n                <!-- sources -->\n                <div class=\"scandal-category\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <div class=\"occurance\">\n                        <p href=\"\">October 6, 2017</p>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                </div>\n                <!-- scandal-category -->\n            </div>\n            <!-- description -->\n        </div>\n        <!-- grid-one-big -->\n        <div class=\"atw-wrapper\">\n            <div class=\"nextbtn\">\n                <button><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></button>\n            </div>\n            <div class=\"preButton\">\n                <button>1 of 4 <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></button>\n            </div>\n            <div class=\"news1\">\n                <div class=\"grid-one\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <h3>This is a news title</h3>\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">October 6, 2017</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n            <!-- news1 -->\n            <div class=\"news1\">\n                <div class=\"grid-one\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <h3>This is a news title</h3>\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">October 6, 2017</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n            <!-- news1 -->\n            <div class=\"news2\">\n                <div class=\"grid-one\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <h3>This is a news title</h3>\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">October 6, 2017</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n            <!-- news2 -->\n            <div class=\"news2\">\n                <div class=\"grid-one\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <h3>This is a news title</h3>\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">October 6, 2017</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n            <!-- news2 -->\n        </div>\n        <!-- atw-wrapper -->\n    </div>\n</div>\n<!-- World-news -->\n<div id=\"around-the-world\" class=\"container\">\n    <h3>Around the world</h3>\n    <div class=\"underline\"></div>\n    <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n    <div class=\"atw-wrapper\">\n        <div class=\"nextbtn\">\n            <button><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></button>\n        </div>\n        <div class=\"preButton\">\n            <button><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></button>\n        </div>\n        <div class=\"news1\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <div class=\"news1\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <div class=\"news2\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news2 -->\n        <div class=\"news2\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news2 -->\n    </div>\n    <!-- atw-wrapper -->\n</div>\n<!-- around-the-world -->\n<div class=\"container\">\n    <div class=\"adblock\">\n        <img src=\"http://placehold.it/1140x266\">\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/category-main/category-main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryMainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CategoryMainComponent = (function () {
    function CategoryMainComponent() {
    }
    CategoryMainComponent.prototype.ngOnInit = function () {
    };
    CategoryMainComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-category-main',
            template: __webpack_require__("../../../../../src/app/category-main/category-main.component.html"),
            styles: [__webpack_require__("../../../../../src/app/category-main/category-main.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CategoryMainComponent);
    return CategoryMainComponent;
}());



/***/ }),

/***/ "../../../../../src/app/coroseldemo/coroseldemo.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#container {\n    width: 100%;\n    margin: 5em auto;\n    padding: 0;\n    background: #fff;\n}\n\n.items {\n    max-width: 300px;\n    height: 300px;\n    background: #ECECEC;\n}\n\n#left,\n#right {\n    margin: 30px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/coroseldemo/coroseldemo.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"container\" style=\"margin-top:50px;margin-bottom:50px\">\n\n    <div ng2-carouselamos class=\"slides-wrapper\" [items]=\"data\" [width]=\"1000\" [$prev]=\"prev\" [$next]=\"next\" [$item]=\"item\">\n    </div>\n\n    <ng-template #prev>\n        <img src=\"../../assets/images/left.png\" style=\"height:50px;width:50px\" id=\"left\" />\n    </ng-template>\n\n    <ng-template #next>\n        <img src=\"../../assets/images/right.jpg\" style=\"height:50px;width:50px\" id=\"right\" />\n    </ng-template>\n\n    <div class=\"atw-wrapper\">\n        <div class=\"card\">\n            <div class=\"card-body\">\n                <div class=\"row\" style=\"width:100%; height:100%;\">\n                    <ng-template #item let-item let-i=\"index\">\n                        <div class=\"items\">\n                            <div class=\"news1 row\">\n                                <div class=\"grid-one col-sm-6\">\n                                    <img src={{item.image}}>\n                                    <div class=\"contents\">\n                                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                                        <div class=\"sources\">\n                                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                                            <button id=\"source2\"> Source 1</button>\n                                            <button id=\"source3\"> Source 1</button>\n                                        </div>\n                                    </div>\n                                    <!-- contents -->\n                                </div>\n                                <!-- grid-one -->\n                                <div class=\"grid-two col-sm-6\">\n                                    <div class=\"category green\">\n                                        <a href=\"politics\">Politics</a>\n                                    </div>\n                                    <!-- category green -->\n                                    <h3>{{item.title}}</h3>\n                                    <div class=\"occurance\">\n                                        <a class=\"grey\" href=\"\">{{item.occurance}}}</a>\n                                    </div>\n                                    <!-- occurance -->\n                                    <div class=\"views grey\">\n                                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{item.views}}}</p>\n                                    </div>\n                                    <!-- views grey -->\n                                    <div class=\"bookmark\">\n                                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                                    </div>\n                                    <!-- bookmark -->\n                                    <div class=\"description\">\n                                        <p>{{item.description}}</p>\n                                    </div>\n                                    <!-- description -->\n                                </div>\n                                <!-- grid-two -->\n                            </div>\n                        </div>\n                    </ng-template>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/coroseldemo/coroseldemo.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoroseldemoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__around_world_service__ = __webpack_require__("../../../../../src/app/around-world.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CoroseldemoComponent = (function () {
    function CoroseldemoComponent(AroundWorldService) {
        this.AroundWorldService = AroundWorldService;
        this.items = [];
        this.data = [];
        this.items = [
            { name: '../../assets/images/manotr-logo.gif' },
            { name: '../../assets/images/manotr-logo.png' },
            { name: '../../assets/images/manotr-logo.gif' },
            { name: '../../assets/images/manotr-logo.png' },
            { name: '../../assets/images/manotr-logo.gif' },
            { name: '../../assets/images/manotr-logo.png' },
            { name: '../../assets/images/manotr-logo.gif' },
            { name: '../../assets/images/manotr-logo.png' },
            { name: '../../assets/images/manotr-logo.gif' },
            { name: '../../assets/images/manotr-logo.png' },
            { name: '../../assets/images/manotr-logo.gif' },
            { name: '../../assets/images/manotr-logo.png' },
        ];
        this.data = this.AroundWorldService.getAroundWordNews();
    }
    CoroseldemoComponent.prototype.ngOnInit = function () {
    };
    CoroseldemoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-coroseldemo',
            template: __webpack_require__("../../../../../src/app/coroseldemo/coroseldemo.component.html"),
            styles: [__webpack_require__("../../../../../src/app/coroseldemo/coroseldemo.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__around_world_service__["a" /* AroundWorldService */]])
    ], CoroseldemoComponent);
    return CoroseldemoComponent;
}());



/***/ }),

/***/ "../../../../../src/app/cricket/cricket.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/cricket/cricket.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/cricket/cricket.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CricketComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CricketComponent = (function () {
    function CricketComponent() {
    }
    CricketComponent.prototype.ngOnInit = function () {
    };
    CricketComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-cricket',
            template: __webpack_require__("../../../../../src/app/cricket/cricket.component.html"),
            styles: [__webpack_require__("../../../../../src/app/cricket/cricket.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CricketComponent);
    return CricketComponent;
}());



/***/ }),

/***/ "../../../../../src/app/finance/finance.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/finance/finance.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/finance/finance.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FinanceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FinanceComponent = (function () {
    function FinanceComponent() {
    }
    FinanceComponent.prototype.ngOnInit = function () {
    };
    FinanceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-finance',
            template: __webpack_require__("../../../../../src/app/finance/finance.component.html"),
            styles: [__webpack_require__("../../../../../src/app/finance/finance.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FinanceComponent);
    return FinanceComponent;
}());



/***/ }),

/***/ "../../../../../src/app/gaming/gaming.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/gaming/gaming.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/gaming/gaming.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GamingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GamingComponent = (function () {
    function GamingComponent() {
    }
    GamingComponent.prototype.ngOnInit = function () {
    };
    GamingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-gaming',
            template: __webpack_require__("../../../../../src/app/gaming/gaming.component.html"),
            styles: [__webpack_require__("../../../../../src/app/gaming/gaming.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], GamingComponent);
    return GamingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/jquerycorosel/jquerycorosel.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/jquerycorosel/jquerycorosel.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n    jquerycorosel works!\n</p>\n\n\n<h3>Around the world</h3>\n\n<button class=\"view-all\" routerLink=\"/news-list\">  view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n<div class=\"atw-wrapper\">\n    <section class=\"regular slider\">\n        <div class=\"card\">\n            <div class=\"card-body\">\n                <div class=\"col-sm-12\" style=\"width:800px; height:500px;\">\n                    <div class=\"news1\" *ngFor=\"let item of data; let i = index\" routerLink=\"/news-landing\">\n                        <div class=\"grid-one col-md-6\">\n                            <img src={{item.image}}>\n                            <div class=\"contents\">\n                                <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                                <div class=\"sources\">\n                                    <button id=\"source1\" class=\"active\"> Source 1</button>\n                                    <button id=\"source2\"> Source 1</button>\n                                    <button id=\"source3\"> Source 1</button>\n                                </div>\n                            </div>\n                            <!-- contents -->\n                        </div>\n                        <!-- grid-one -->\n                        <div class=\"grid-two col-md-6\">\n                            <div class=\"category green\">\n                                <a href=\"politics\">Politics</a>\n                            </div>\n                            <!-- category green -->\n                            <h3>{{item.title}}</h3>\n                            <div class=\"occurance\">\n                                <a class=\"grey\" href=\"\">{{item.occurance}}}</a>\n                            </div>\n                            <!-- occurance -->\n                            <div class=\"views grey\">\n                                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{item.views}}}</p>\n                            </div>\n                            <!-- views grey -->\n                            <div class=\"bookmark\">\n                                <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                            </div>\n                            <!-- bookmark -->\n                            <div class=\"description\">\n                                <p>{{item.description}}</p>\n                            </div>\n                            <!-- description -->\n                        </div>\n                    </div>\n                </div>\n                <!-- grid-two -->\n            </div>\n        </div>\n    </section>\n</div>"

/***/ }),

/***/ "../../../../../src/app/jquerycorosel/jquerycorosel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JquerycoroselComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__around_world_service__ = __webpack_require__("../../../../../src/app/around-world.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var JquerycoroselComponent = (function () {
    function JquerycoroselComponent(AroundWorldService) {
        this.AroundWorldService = AroundWorldService;
        this.data = '';
    }
    JquerycoroselComponent.prototype.ngOnInit = function () {
        this.data = this.AroundWorldService.getAroundWordNews();
        console.log(this.data);
        $(".regular").slick({
            dots: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3
        });
    };
    JquerycoroselComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-jquerycorosel',
            template: __webpack_require__("../../../../../src/app/jquerycorosel/jquerycorosel.component.html"),
            styles: [__webpack_require__("../../../../../src/app/jquerycorosel/jquerycorosel.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__around_world_service__["a" /* AroundWorldService */]])
    ], JquerycoroselComponent);
    return JquerycoroselComponent;
}());



/***/ }),

/***/ "../../../../../src/app/login-signup/login-signup.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login-signup/login-signup.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"login-background\"></div>\n<!-- login-background -->\n<div class=\"login-div\">\n    <h3>Join Manotr</h3>\n    <p>Please sign in or create an account to avail our special features</p>\n    <div class=\"social-buttons\" aria-hidden=\"true\">\n        <i class=\"fa fa-facebook fb social-btn\" aria-hidden=\"true\"></i>\n        <i class=\"fa fa-twitter t social-btn\" aria-hidden=\"true\"></i>\n        <i class=\"fa fa-google-plus g social-btn\" aria-hidden=\"true\"></i>\n    </div>\n    <!--  social-buttons -->\n    <p>or via email</p>\n    <div class=\"other-div\">\n        <div>\n            <i class=\"fa fa-user\" aria-hidden=\"true\"></i><input type=\"text\" class=\"name textbox\" name=\"\" placeholder=\"Full Name\">\n        </div>\n        <div>\n            <i>@</i>&nbsp;<input type=\"email\" name=\"\" class=\"email textbox\" placeholder=\"Email\">\n        </div>\n        <div>\n            <i class=\"fa fa-lock\" aria-hidden=\"true\"></i><input type=\"Password\" name=\"\" class=\"textbox password\" placeholder=\"Password\">\n        </div>\n        <div>\n            <button class=\"sign-up\">Sign Up</button>\n        </div>\n    </div>\n    <!-- other-div -->\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/login-signup/login-signup.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginSignupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoginSignupComponent = (function () {
    function LoginSignupComponent() {
    }
    LoginSignupComponent.prototype.ngOnInit = function () {
    };
    LoginSignupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-login-signup',
            template: __webpack_require__("../../../../../src/app/login-signup/login-signup.component.html"),
            styles: [__webpack_require__("../../../../../src/app/login-signup/login-signup.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LoginSignupComponent);
    return LoginSignupComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-headlines.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainHeadlinesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainHeadlinesService = (function () {
    function MainHeadlinesService() {
        this.mainHeadlines = [
            {
                sources: [
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 1, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news 1",
                        showdate: "Octombor 6,2017",
                        views: "5",
                    },
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 3, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news 2",
                        showdate: "Octombor 7,2017",
                        views: "80",
                    },
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 3, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news 3",
                        showdate: "Octombor 8,2017",
                        views: "75",
                    }
                ]
            },
            {
                sources: [
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 11, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 6,2017",
                        views: "95",
                    },
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 12, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 7,2017",
                        views: "85",
                    },
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 13, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 8,2017",
                        views: "15",
                    }
                ]
            },
            {
                sources: [
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 21, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 6,2017",
                        views: "85",
                    },
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 21, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 7,2017",
                        views: "85",
                    },
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 23, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 8,2017",
                        views: "85",
                    }
                ]
            },
            {
                sources: [
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 31, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 6,2017",
                        views: "85",
                    },
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 32, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 7,2017",
                        views: "85",
                    },
                    {
                        image: "http://www.lorempixel.com/315/478",
                        description: "Patrick Bordier 33, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                        title: "demo news",
                        showdate: "Octombor 8,2017",
                        views: "85",
                    }
                ]
            }
        ];
    }
    MainHeadlinesService.prototype.getHeadlines = function () {
        console.log("come here in news desk servie");
        return this.mainHeadlines;
    };
    MainHeadlinesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], MainHeadlinesService);
    return MainHeadlinesService;
}());



/***/ }),

/***/ "../../../../../src/app/main-headlines/main-headlines.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#headline:hover {\r\n    cursor: pointer;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-headlines/main-headlines.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"main-headlines\" class=\"col-sm-12\">\r\n    <div *ngFor=\"let item of data; let i = index\" id=\"headline\" class=\"col-sm-3\" style=\"padding-right: 0px;padding-left:0px; cursor: hand;\" routerLink=\"/news-landing\">\r\n\r\n        <div [style.display]=\"j==0?'block':'none'\" *ngFor=\"let subitem of item.sources;let j = index\" id=\"imgsource{{j}}\">\r\n            <div class=\"overlay\"></div>\r\n            <div id=\"header-image\">\r\n                <img src={{subitem.image}} title=\"\">\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"news-part\">\r\n            <div [style.display]=\"k==0?'block':'none'\" *ngFor=\"let subitem of item.sources;let k = index\" id=\"detailsource{{k}}\">\r\n                <div class=\"category pink\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <div class=\"description\">\r\n                    <p>{{subitem.description}}</p>\r\n                </div>\r\n                <div class=\"occurance\">\r\n                    <a href=\"\">{{subitem.showdate}}</a>\r\n                </div>\r\n                <div class=\"views\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{subitem.views}}</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"sources\">\r\n                <!-- <button *ngFor=\"let subitem of item.sources;let l = index\" id=\"btnsource{{l}}\" [attr.class]=\"l==0?'active':''\">Source {{l}}</button> -->\r\n                <button id=\"btnsource0\" class=\"active\">Source 1</button>\r\n                <button id=\"btnsource1\">Source 2</button>\r\n                <button id=\"btnsource2\">Source 3</button>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <!-- headline-1 -->\r\n</div>\r\n<!-- main-headlines -->"

/***/ }),

/***/ "../../../../../src/app/main-headlines/main-headlines.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainHeadlinesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__main_headlines_service__ = __webpack_require__("../../../../../src/app/main-headlines.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MainHeadlinesComponent = (function () {
    function MainHeadlinesComponent(headlinesSercice) {
        this.headlinesSercice = headlinesSercice;
        this.data = '';
        this.activeBtn = 0;
        this.timer = '';
    }
    MainHeadlinesComponent.prototype.ngOnInit = function () {
        this.data = this.headlinesSercice.getHeadlines();
        console.log(this.data);
    };
    MainHeadlinesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-main-headlines',
            template: __webpack_require__("../../../../../src/app/main-headlines/main-headlines.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-headlines/main-headlines.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__main_headlines_service__["a" /* MainHeadlinesService */]])
    ], MainHeadlinesComponent);
    return MainHeadlinesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/news-desk.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsDeskService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewsDeskService = (function () {
    function NewsDeskService() {
        this.newsDeskData = [
            {
                image: "http://www.lorempixel.com/315/478",
                title: "demo news",
                description: "Patrick Bordier, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                occurance: "Octombor 6,2017",
                views: "85",
                sources: "demo"
            },
            {
                image: "http://www.lorempixel.com/315/478",
                title: "demo news",
                description: "Patrick Bordier, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                occurance: "Octombor 6,2017",
                views: "85",
                sources: "demo"
            },
            {
                image: "http://www.lorempixel.com/315/478",
                title: "demo news",
                description: "Patrick Bordier, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
                occurance: "Octombor 6,2017",
                views: "85",
                sources: "demo"
            }
        ];
    }
    NewsDeskService.prototype.getNewsDews = function () {
        console.log("come here in news desk servie");
        return this.newsDeskData;
    };
    NewsDeskService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], NewsDeskService);
    return NewsDeskService;
}());



/***/ }),

/***/ "../../../../../src/app/news-desk/news-desk.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".news1,\n.grid-one:hover {\n    cursor: pointer;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-desk/news-desk.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"news-desk\">\n    <h3>NEWS DESK</h3>\n    <div class=\"underline\"></div>\n    <button class=\"view-all\" routerLink=\"/category-listing\"> view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n    <div class=\"news1\" routerLink=\"/news-landing\">\n        <div class=\"grid-one\">\n            <img src=\"http://www.lorempixel.com/270/180\">\n            <div class=\"contents\">\n                <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                <div class=\"sources\">\n                    <button id=\"source1\" class=\"active\" routerLink=\"/news-landing\"> Source 1</button>\n                    <button id=\"source2\" routerLink=\"/news-landing\"> Source 1</button>\n                    <button id=\"source3\" routerLink=\"/news-landing\"> Source 1</button>\n                </div>\n            </div>\n            <!-- contents -->\n        </div>\n        <!-- grid-one -->\n        <div class=\"grid-two\">\n            <div class=\"category green\">\n                <a href=\"politics\">Politics</a>\n            </div>\n            <!-- category green -->\n            <h3>This is a news title</h3>\n            <div class=\"occurance\">\n                <a class=\"grey\" href=\"\">October 6, 2017</a>\n            </div>\n            <!-- occurance -->\n            <div class=\"views grey\">\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n            </div>\n            <!-- views grey -->\n            <div class=\"bookmark\">\n                <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n            </div>\n            <!-- bookmark -->\n            <div class=\"description\">\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n            </div>\n            <!-- description -->\n        </div>\n        <!-- grid-two -->\n    </div>\n    <!-- news1 -->\n    <hr>\n    <div class=\"news2\" routerLink=\"/news-landing\">\n        <div class=\"grid-one\">\n            <div class=\"category green\">\n                <a href=\"politics\">Politics</a>\n            </div>\n            <!-- category green -->\n            <h3>This is a news title</h3>\n            <div class=\"occurance\">\n                <a class=\"grey\" href=\"\">October 6, 2017</a>\n            </div>\n            <!-- occurance -->\n            <div class=\"views grey\">\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n            </div>\n            <!-- views grey -->\n            <div class=\"bookmark\">\n                <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n            </div>\n            <!-- bookmark -->\n            <div class=\"description\">\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n            </div>\n            <!-- description -->\n        </div>\n        <div class=\"grid-two\">\n            <img src=\"http://www.lorempixel.com/270/180\">\n            <div class=\"contents\">\n                <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                <div class=\"sources\">\n                    <button id=\"source1\" class=\"active\" routerLink=\"/news-landing\"> Source 1</button>\n                    <button id=\"source2\" routerLink=\"/news-landing\"> Source 1</button>\n                    <button id=\"source3\" routerLink=\"/news-landing\"> Source 1</button>\n                </div>\n            </div>\n            <!-- contents -->\n        </div>\n        <!-- grid-two -->\n    </div>\n    <!-- news2 -->\n    <hr>\n    <div class=\"news1\" routerLink=\"/news-landing\">\n        <div class=\"grid-one\">\n            <img src=\"http://www.lorempixel.com/270/180\">\n            <div class=\"contents\">\n                <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                <div class=\"sources\">\n                    <button id=\"source1\" class=\"active\" routerLink=\"/news-landing\"> Source 1</button>\n                    <button id=\"source2\" routerLink=\"/news-landing\"> Source 1</button>\n                    <button id=\"source3\" routerLink=\"/news-landing\"> Source 1</button>\n                </div>\n            </div>\n            <!-- contents -->\n        </div>\n        <!-- grid-one -->\n        <div class=\"grid-two\">\n            <div class=\"category green\">\n                <a href=\"politics\">Politics</a>\n            </div>\n            <!-- category green -->\n            <h3>This is a news title</h3>\n            <div class=\"occurance\">\n                <a class=\"grey\" href=\"\">October 6, 2017</a>\n            </div>\n            <!-- occurance -->\n            <div class=\"views grey\">\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n            </div>\n            <!-- views grey -->\n            <div class=\"bookmark\">\n                <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n            </div>\n            <!-- bookmark -->\n            <div class=\"description\">\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n            </div>\n            <!-- description -->\n        </div>\n        <!-- grid-two -->\n    </div>\n    <!-- news1 -->\n</div>\n<!-- news-desk -->"

/***/ }),

/***/ "../../../../../src/app/news-desk/news-desk.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsDeskComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__news_desk_service__ = __webpack_require__("../../../../../src/app/news-desk.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NewsDeskComponent = (function () {
    function NewsDeskComponent(NewsDeskService) {
        this.NewsDeskService = NewsDeskService;
        this.data = '';
    }
    NewsDeskComponent.prototype.ngOnInit = function () {
        this.data = this.NewsDeskService.getNewsDews();
        console.log(this.data);
    };
    NewsDeskComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-news-desk',
            template: __webpack_require__("../../../../../src/app/news-desk/news-desk.component.html"),
            styles: [__webpack_require__("../../../../../src/app/news-desk/news-desk.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__news_desk_service__["a" /* NewsDeskService */]])
    ], NewsDeskComponent);
    return NewsDeskComponent;
}());



/***/ }),

/***/ "../../../../../src/app/news-landing-graph/news-landing-graph.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-landing-graph/news-landing-graph.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  news-landing-graph works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/news-landing-graph/news-landing-graph.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsLandingGraphComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewsLandingGraphComponent = (function () {
    function NewsLandingGraphComponent() {
    }
    NewsLandingGraphComponent.prototype.ngOnInit = function () {
    };
    NewsLandingGraphComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-news-landing-graph',
            template: __webpack_require__("../../../../../src/app/news-landing-graph/news-landing-graph.component.html"),
            styles: [__webpack_require__("../../../../../src/app/news-landing-graph/news-landing-graph.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NewsLandingGraphComponent);
    return NewsLandingGraphComponent;
}());



/***/ }),

/***/ "../../../../../src/app/news-landing-news-details/news-landing-news-details.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-landing-news-details/news-landing-news-details.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-grid\">\n    <div class=\"left-grid-main\">\n\n        <h2>{{data.title}}</h2>\n        <div class=\"occurance\">\n            <p class=\"grey\" href=\"\">{{data.occurance}}</p>\n        </div>\n        <!-- occurance -->\n        <div class=\"bookmark clearfix\">\n            <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n            <div class=\"social-icons\">\n                <i class=\"fa fa-facebook social-button fb-button\" aria-hideen=\"true\"> Share</i>\n                <i class=\"fa fa-twitter social-button t-button\" aria-hideen=\"true\"> Tweet</i>\n            </div>\n            <!-- socual-icons -->\n        </div>\n        <!-- bookmark -->\n\n        <div class=\"news-list\">\n            <div class=\"left-grid\">\n                <img src=\"{{data.image}}\">\n            </div>\n            <!-- left-grid -->\n            <div class=\"right-grid\">\n                <div class=\"right-description\">\n                    {{data.description}}\n                </div>\n                <div class=\"bookmark-readmore\">\n                    <a href=\"{{data.redMoreLink}}\">&nbsp;READ FULL STORY </a><i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i>\n                </div>\n                <!-- bookmark -->\n\n            </div>\n            <!-- right-grid -->\n        </div>\n        <!-- news-left -->\n        <div class=\"tags\">\n            <ul>\n                <li><i class=\"fa fa-tag\" aria-hidden=\"true\"></i></li>\n                <li>Tag 1</li>\n                <li>Tag 2</li>\n                <li>Tag 3</li>\n                <li>Tag 4</li>\n                <li>Tag 5</li>\n            </ul>\n        </div>\n        <div class=\"right-grid\">\n\n        </div>\n        <!-- right-grid -->\n    </div>\n    <!-- news-list -->\n    <img src=\"http://placehold.it/700x150\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/news-landing-news-details/news-landing-news-details.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsLandingNewsDetailsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__newslanding_news_details_service__ = __webpack_require__("../../../../../src/app/newslanding-news-details.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NewsLandingNewsDetailsComponent = (function () {
    function NewsLandingNewsDetailsComponent(newsdetails) {
        this.newsdetails = newsdetails;
        this.data = '';
    }
    NewsLandingNewsDetailsComponent.prototype.ngOnInit = function () {
        this.data = this.newsdetails.getNewsDetails();
        console.log("data" + this.data);
    };
    NewsLandingNewsDetailsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-news-landing-news-details',
            template: __webpack_require__("../../../../../src/app/news-landing-news-details/news-landing-news-details.component.html"),
            styles: [__webpack_require__("../../../../../src/app/news-landing-news-details/news-landing-news-details.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__newslanding_news_details_service__["a" /* NewslandingNewsDetailsService */]])
    ], NewsLandingNewsDetailsComponent);
    return NewsLandingNewsDetailsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/news-landing-related-news-bottom-div/news-landing-related-news-bottom-div.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-landing-related-news-bottom-div/news-landing-related-news-bottom-div.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"related-news\">\n\n    <div class=\"grid-one\" *ngFor=\"let item of data\" routerLink=\"news-landing\">\n        <div class=\"banner-image\">\n            <img src=\"{{item.image}}\">\n            <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n            <div class=\"lower-buttons\">\n                <div class=\"sources\">\n                    <button id=\"source1\" class=\"active\"> Source 1</button>\n                    <button id=\"source2\"> Source 1</button>\n                    <button id=\"source3\"> Source 1</button>\n                    <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                </div>\n            </div>\n        </div>\n        <div class=\"banner-contents\">\n            <div class=\"category green\">\n                <a href=\"politics\">Politics</a>\n            </div>\n            <!-- category green -->\n            <div class=\"description\">\n                <p>{{item.description}}}</p>\n            </div>\n            <div class=\"occurance\">\n                <p class=\"grey\" href=\"\">{{item.occurance}}</p>\n            </div>\n            <!-- occurance -->\n            <div class=\"views grey\">\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{item.views}}</p>\n            </div>\n            <!-- views grey -->\n        </div>\n        <!-- contents -->\n    </div>\n    <!-- grid-one -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/news-landing-related-news-bottom-div/news-landing-related-news-bottom-div.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsLandingRelatedNewsBottomDivComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__newslanding_bottomdivnews_service__ = __webpack_require__("../../../../../src/app/newslanding-bottomdivnews.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NewsLandingRelatedNewsBottomDivComponent = (function () {
    function NewsLandingRelatedNewsBottomDivComponent(relatedNews) {
        this.relatedNews = relatedNews;
        this.data = '';
    }
    NewsLandingRelatedNewsBottomDivComponent.prototype.ngOnInit = function () {
        this.data = this.relatedNews.getRelatedNews();
        console.log(this.data);
    };
    NewsLandingRelatedNewsBottomDivComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-news-landing-related-news-bottom-div',
            template: __webpack_require__("../../../../../src/app/news-landing-related-news-bottom-div/news-landing-related-news-bottom-div.component.html"),
            styles: [__webpack_require__("../../../../../src/app/news-landing-related-news-bottom-div/news-landing-related-news-bottom-div.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__newslanding_bottomdivnews_service__["a" /* NewslandingBottomdivnewsService */]])
    ], NewsLandingRelatedNewsBottomDivComponent);
    return NewsLandingRelatedNewsBottomDivComponent;
}());



/***/ }),

/***/ "../../../../../src/app/news-landing-related-news-right.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsLandingRelatedNewsRightService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewsLandingRelatedNewsRightService = (function () {
    function NewsLandingRelatedNewsRightService() {
        this.relatedNews = [{
                image: "http://www.placehold.it/100x80",
                occurance: "October 6, 2017",
                views: "1465",
                discription: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            },
            {
                image: "http://www.placehold.it/100x80",
                occurance: "October 6, 2017",
                views: "1465",
                discription: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            },
            {
                image: "http://www.placehold.it/100x80",
                occurance: "October 6, 2017",
                views: "1465",
                discription: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            },
            {
                image: "http://www.placehold.it/100x80",
                occurance: "October 6, 2017",
                views: "1465",
                discription: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            },
            {
                image: "http://www.placehold.it/100x80",
                occurance: "October 6, 2017",
                views: "1465",
                discription: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            }];
    }
    NewsLandingRelatedNewsRightService.prototype.getRelatedNews = function () {
        console.log("inside right relates news");
        return this.relatedNews;
    };
    NewsLandingRelatedNewsRightService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], NewsLandingRelatedNewsRightService);
    return NewsLandingRelatedNewsRightService;
}());



/***/ }),

/***/ "../../../../../src/app/news-landing-related-news-rightt-div/news-landing-related-news-rightt-div.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-landing-related-news-rightt-div/news-landing-related-news-rightt-div.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"side-grid\">\n    <h4>Related News From Politics</h4>\n    <div class=\"underline\"></div>\n    <div class=\"news1\" *ngFor=\"let item of data; let i=index\" routerLink=\"news-landing\">\n        <div class=\"grid-one\">\n            <img src=\"{{item.image}}\">\n        </div>\n        <!-- grid-one -->\n        <div class=\"grid-two\">\n            <div class=\"description\">\n                <p>{{item.discription}}</p>\n            </div>\n            <!-- description -->\n            <div class=\"occurance\">\n                <p class=\"grey\" href=\"\">{{item.occurance}}</p>\n            </div>\n            <!-- occurance -->\n            <div class=\"views grey\">\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{item.views}}</p>\n            </div>\n            <!-- views grey -->\n        </div>\n        <!-- grid-two -->\n    </div>\n    <!-- news1 -->\n\n    <button class=\"view-all\" routerLink=\"/category-listing\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n</div>\n<!-- side-grid -->"

/***/ }),

/***/ "../../../../../src/app/news-landing-related-news-rightt-div/news-landing-related-news-rightt-div.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsLandingRelatedNewsRighttDivComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__news_landing_related_news_right_service__ = __webpack_require__("../../../../../src/app/news-landing-related-news-right.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NewsLandingRelatedNewsRighttDivComponent = (function () {
    function NewsLandingRelatedNewsRighttDivComponent(relatedNews) {
        this.relatedNews = relatedNews;
        this.data = '';
    }
    NewsLandingRelatedNewsRighttDivComponent.prototype.ngOnInit = function () {
        this.data = this.relatedNews.getRelatedNews();
        console.log(this.data);
    };
    NewsLandingRelatedNewsRighttDivComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-news-landing-related-news-rightt-div',
            template: __webpack_require__("../../../../../src/app/news-landing-related-news-rightt-div/news-landing-related-news-rightt-div.component.html"),
            styles: [__webpack_require__("../../../../../src/app/news-landing-related-news-rightt-div/news-landing-related-news-rightt-div.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__news_landing_related_news_right_service__["a" /* NewsLandingRelatedNewsRightService */]])
    ], NewsLandingRelatedNewsRighttDivComponent);
    return NewsLandingRelatedNewsRighttDivComponent;
}());



/***/ }),

/***/ "../../../../../src/app/news-landing-tags/news-landing-tags.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-landing-tags/news-landing-tags.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  news-landing-tags works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/news-landing-tags/news-landing-tags.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsLandingTagsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewsLandingTagsComponent = (function () {
    function NewsLandingTagsComponent() {
    }
    NewsLandingTagsComponent.prototype.ngOnInit = function () {
    };
    NewsLandingTagsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-news-landing-tags',
            template: __webpack_require__("../../../../../src/app/news-landing-tags/news-landing-tags.component.html"),
            styles: [__webpack_require__("../../../../../src/app/news-landing-tags/news-landing-tags.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NewsLandingTagsComponent);
    return NewsLandingTagsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/news-landing/news-landing.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".news1,\nnews2,\nnews3:hover {\n    cursor: pointer;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-landing/news-landing.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container main-grid-wrapper clearfix\">\n    <app-news-landing-news-details></app-news-landing-news-details>\n    <!-- main-grid -->\n    <app-news-landing-related-news-rightt-div></app-news-landing-related-news-rightt-div>\n    <!-- side-grid -->\n</div>\n<!-- container -->\n<app-news-landing-related-news-bottom-div></app-news-landing-related-news-bottom-div>"

/***/ }),

/***/ "../../../../../src/app/news-landing/news-landing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsLandingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewsLandingComponent = (function () {
    function NewsLandingComponent() {
    }
    NewsLandingComponent.prototype.ngOnInit = function () {
    };
    NewsLandingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-news-landing',
            template: __webpack_require__("../../../../../src/app/news-landing/news-landing.component.html"),
            styles: [__webpack_require__("../../../../../src/app/news-landing/news-landing.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NewsLandingComponent);
    return NewsLandingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/news-list/news-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/news-list/news-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"categoryBackground\"></div>\n<div id=\"section-two\" class=\"container\">\n    <div class=\"topic-name\">\n        <h2>Politics</h2>\n    </div>\n    <div id=\"scandal-watch\">\n        <h2>HeadLines</h2>\n        <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n        <div class=\"underline\"></div>\n        <div class=\"grid-one-big\">\n            <!-- <div class=\"banner-image\">\n\t\t\t\t\t\t\t<img src=\"http://placehold.it/550x225\">\n\t\t\t\t\t\t\t<div class=\"banner-overlay\"></div>\n\t\t\t\t\t\t</div> -->\n            <div class=\"slider\">\n                <div><img src=\"http://placehold.it/550x225\"></div>\n                <div><img src=\"http://placehold.it/550x225\"></div>\n                <div><img src=\"http://placehold.it/550x225\"></div>\n                <div><img src=\"http://placehold.it/550x225\"></div>\n            </div>\n        </div>\n        <!-- grid-one-big -->\n        <h3>THINK TANK</h3>\n        <div class=\"underline\"></div>\n        <div class=\"think-tank\">\n            <div class=\"grid-one\">\n                <div class=\"banner-image\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"lower-buttons\">\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"banner-contents\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <div class=\"description\">\n                        <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many\n                            presidential campaigns. Some candidates survived. Other didn't</p>\n                    </div>\n                    <div class=\"occurance\">\n                        <p class=\"grey\" href=\"\">October 6, 2017</p>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-one\">\n                <div class=\"banner-image\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"lower-buttons\">\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"banner-contents\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <div class=\"description\">\n                        <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many\n                            presidential campaigns. Some candidates survived. Other didn't</p>\n                    </div>\n                    <div class=\"occurance\">\n                        <p class=\"grey\" href=\"\">October 6, 2017</p>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n        </div>\n    </div>\n    <!-- scandal-watch -->\n    <div id=\"news-desk\">\n        <h2>NEWS DESK</h2>\n        <div class=\"underline\"></div>\n        <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n        <div class=\"news1\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <hr>\n        <div class=\"news2\">\n            <div class=\"grid-one\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <div class=\"grid-two\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news2 -->\n        <hr>\n        <div class=\"news1\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n    </div>\n    <!-- news-desk -->\n</div>\n<!-- section-two -->\n<div class=\"world-news\">\n    <div class=\"container\">\n        <h2>World news</h2>\n        <div class=\"underline\"></div>\n        <div class=\"grid-one-big\">\n            <div class=\"banner-image\">\n                <img src=\"http://placehold.it/1140x466\">\n                <div class=\"banner-overlay\"></div>\n            </div>\n            <div class=\"description\">\n                <h3>\"</h3>\n                <h3>HISTORIC PRESIDENTIAL CAMPAIGN Scandal</h3>\n                <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many presidential\n                    campaigns. Some candidates survived. Other didn't</p>\n                <div class=\"sources\">\n                    <button id=\"source1\" class=\"active\"> Source 1</button>\n                    <button id=\"source2\"> Source 1</button>\n                    <button id=\"source3\"> Source 1</button>\n                    <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                </div>\n                <!-- sources -->\n                <div class=\"scandal-category\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <div class=\"occurance\">\n                        <p href=\"\">October 6, 2017</p>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                </div>\n                <!-- scandal-category -->\n            </div>\n            <!-- description -->\n        </div>\n        <!-- grid-one-big -->\n        <div class=\"atw-wrapper\">\n            <div class=\"nextbtn\">\n                <button><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></button>\n            </div>\n            <div class=\"preButton\">\n                <button>1 of 4 <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></button>\n            </div>\n            <div class=\"news1\">\n                <div class=\"grid-one\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <h3>This is a news title</h3>\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">October 6, 2017</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n            <!-- news1 -->\n            <div class=\"news1\">\n                <div class=\"grid-one\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <h3>This is a news title</h3>\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">October 6, 2017</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n            <!-- news1 -->\n            <div class=\"news2\">\n                <div class=\"grid-one\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <h3>This is a news title</h3>\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">October 6, 2017</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n            <!-- news2 -->\n            <div class=\"news2\">\n                <div class=\"grid-one\">\n                    <img src=\"http://www.placehold.it/270x180\">\n                    <div class=\"contents\">\n                        <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                        <div class=\"sources\">\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\n                            <button id=\"source2\"> Source 1</button>\n                            <button id=\"source3\"> Source 1</button>\n                            <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                        </div>\n                    </div>\n                    <!-- contents -->\n                </div>\n                <!-- grid-one -->\n                <div class=\"grid-two\">\n                    <div class=\"category green\">\n                        <a href=\"politics\">Politics</a>\n                    </div>\n                    <!-- category green -->\n                    <h3>This is a news title</h3>\n                    <div class=\"occurance\">\n                        <a class=\"grey\" href=\"\">October 6, 2017</a>\n                    </div>\n                    <!-- occurance -->\n                    <div class=\"views grey\">\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                    </div>\n                    <!-- views grey -->\n                    <div class=\"bookmark\">\n                        <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                    </div>\n                    <!-- bookmark -->\n                    <div class=\"description\">\n                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                    </div>\n                    <!-- description -->\n                </div>\n                <!-- grid-two -->\n            </div>\n            <!-- news2 -->\n        </div>\n        <!-- atw-wrapper -->\n    </div>\n</div>\n<!-- World-news -->\n<div id=\"around-the-world\" class=\"container\">\n    <h3>Around the world</h3>\n    <div class=\"underline\"></div>\n    <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n    <div class=\"atw-wrapper\">\n        <div class=\"nextbtn\">\n            <button><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></button>\n        </div>\n        <div class=\"preButton\">\n            <button><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></button>\n        </div>\n        <div class=\"news1\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <div class=\"news1\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news1 -->\n        <div class=\"news2\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news2 -->\n        <div class=\"news2\">\n            <div class=\"grid-one\">\n                <img src=\"http://www.placehold.it/270x180\">\n                <div class=\"contents\">\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                    <div class=\"sources\">\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\n                        <button id=\"source2\"> Source 1</button>\n                        <button id=\"source3\"> Source 1</button>\n                        <i id=\"playButton\" class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i>\n                    </div>\n                </div>\n                <!-- contents -->\n            </div>\n            <!-- grid-one -->\n            <div class=\"grid-two\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <h3>This is a news title</h3>\n                <div class=\"occurance\">\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\n                </div>\n                <!-- views grey -->\n                <div class=\"bookmark\">\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\n                </div>\n                <!-- bookmark -->\n                <div class=\"description\">\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\n                </div>\n                <!-- description -->\n            </div>\n            <!-- grid-two -->\n        </div>\n        <!-- news2 -->\n    </div>\n    <!-- atw-wrapper -->\n</div>\n<!-- around-the-world -->\n<div class=\"container\">\n    <div class=\"adblock\">\n        <img src=\"http://placehold.it/1140x266\">\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/news-list/news-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewsListComponent = (function () {
    function NewsListComponent() {
    }
    NewsListComponent.prototype.ngOnInit = function () {
    };
    NewsListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-news-list',
            template: __webpack_require__("../../../../../src/app/news-list/news-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/news-list/news-list.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NewsListComponent);
    return NewsListComponent;
}());



/***/ }),

/***/ "../../../../../src/app/newsdesk-viewdetail/newsdesk-viewdetail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/newsdesk-viewdetail/newsdesk-viewdetail.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/newsdesk-viewdetail/newsdesk-viewdetail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsdeskViewdetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewsdeskViewdetailComponent = (function () {
    function NewsdeskViewdetailComponent() {
    }
    NewsdeskViewdetailComponent.prototype.ngOnInit = function () {
    };
    NewsdeskViewdetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-newsdesk-viewdetail',
            template: __webpack_require__("../../../../../src/app/newsdesk-viewdetail/newsdesk-viewdetail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/newsdesk-viewdetail/newsdesk-viewdetail.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NewsdeskViewdetailComponent);
    return NewsdeskViewdetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/newslanding-bottomdivnews.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewslandingBottomdivnewsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewslandingBottomdivnewsService = (function () {
    function NewslandingBottomdivnewsService() {
        this.relatedNews = [{
                image: "",
                sources: ["source1", "source2", "source3"],
                description: "Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates",
                occurance: "October 6, 2017",
                views: "1465"
            },
            {
                image: "",
                sources: ["source1", "source2", "source3"],
                description: "Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates",
                occurance: "October 6, 2017",
                views: "1465"
            },
            {
                image: "",
                sources: ["source1", "source2", "source3"],
                description: "Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates",
                occurance: "October 6, 2017",
                views: "1465"
            },
            {
                image: "",
                sources: ["source1", "source2", "source3"],
                description: "Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates",
                occurance: "October 6, 2017",
                views: "1465"
            }];
    }
    NewslandingBottomdivnewsService.prototype.getRelatedNews = function () {
        console.log("inside reltaed news bootom news");
        return this.relatedNews;
    };
    NewslandingBottomdivnewsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], NewslandingBottomdivnewsService);
    return NewslandingBottomdivnewsService;
}());



/***/ }),

/***/ "../../../../../src/app/newslanding-news-details.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewslandingNewsDetailsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NewslandingNewsDetailsService = (function () {
    function NewslandingNewsDetailsService() {
        this.newsDetails = {
            sources: [
                "source1",
                "source2",
                "source3"
            ],
            title: "Trump says minorities \'want\' and \'need\' more police protection than other Americans",
            occurance: "October 6, 2017",
            description: "President Trump said Wednesday that minorities \“want\” and “need” more police protection than other Americans, and blamed Democrats for a \“crazy\” number of murders in Chicago and other large cities.In an interview with Sean Hannity, the Fox News Channel anchor who also is a friend and informal adviser to the president, Trump said police in big cities are \“not allowed\” to respond to what he described as rampant crime because \“they have to be politically correct.\”",
            image: "http://placehold.it/300x200",
            redMoreLink: "http://zeenews.india.com/india/gdp-means-gross-divisive-politics-for-narendra-modi-rahul-gandhi-2072232.html",
            tags: [
                "tags1", "tags1",
                "tags1",
                "tags1",
                "tags1",
                "tags1"
            ]
        };
    }
    NewslandingNewsDetailsService.prototype.getNewsDetails = function () {
        console.log("inside news details");
        return this.newsDetails;
    };
    NewslandingNewsDetailsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], NewslandingNewsDetailsService);
    return NewslandingNewsDetailsService;
}());



/***/ }),

/***/ "../../../../../src/app/politics-video/politics-video.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".playbutton {\r\n    position: absolute;\r\n    /* display: none; */\r\n    top: 20%;\r\n    margin: 0 auto;\r\n    left: 45%;\r\n    right: 45%;\r\n    z-index: 100;\r\n    font-size: 2em !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/politics-video/politics-video.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"main-headlines\" class=\"col-sm-12\">\r\n    <div class=\"modal fade\" id=\"videoPlayer\">\r\n        <div class=\"modal-dialog\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\r\n                    <div class=\"sources\">\r\n                        <button id=\"modalSource1\" class=\"active\"> Source 1</button>\r\n                        <button id=\"modalSource2\"> Source 1</button>\r\n                        <button id=\"modalSource3\"> Source 1</button>\r\n                    </div>\r\n                    <h4 class=\"modal-title\">News title</h4>\r\n                    <div class=\"occurance\">\r\n                        <a href=\"\">October 6, 2017</a>\r\n                    </div>\r\n                    <div class=\"views\">\r\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                    </div>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n                    <div class=\"theVideoPlayer\">\r\n                        <video id=\"my-video\" class=\"video-js\" controls preload=\"auto\" width=\"640\" height=\"264\" data-setup=\"{}\">\r\n\t\t\t\t\t\t\t\t\t<source src=\"http://techslides.com/demos/sample-videos/small.mp4\" type='video/mp4'>\r\n\t\t\t\t\t\t\t\t\t<source src=\"http://techslides.com/demos/sample-videos/small.mp4\" type='video/webm'>\r\n\t\t\t\t\t\t\t\t\t<p class=\"vjs-no-js\">\r\n\t\t\t\t\t\t\t\t\t  To view this video please enable JavaScript, and consider upgrading to a web browser that\r\n\t\t\t\t\t\t\t\t\t  <a href=\"http://videojs.com/html5-video-support/\" target=\"_blank\">supports HTML5 video</a>\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t\t</video>\r\n                        <div class=\"video-description\">\r\n                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\r\n                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n                        </div>\r\n                        <!-- video-Description -->\r\n                    </div>\r\n                    <!-- theVideoPlayer -->\r\n                </div>\r\n                <!-- <div class=\"modal-footer\">\r\n\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\r\n\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\">Save changes</button>\r\n\t\t\t\t\t\t\t</div> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div id=\"headline\" class=\"col-sm-3\" style=\"padding-right: 0px;padding-left:0px\">\r\n        <div class=\"overlay\"></div>\r\n        <div id=\"header-image\">\r\n\r\n            <img src=\"http://www.placehold.it/315x478\" title=\"\">\r\n            <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n        </div>\r\n        <div class=\"news-part\">\r\n\r\n            <div class=\"category pink\">\r\n                <a href=\"politics\">Politics</a>\r\n            </div>\r\n            <div class=\"description\">\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n            </div>\r\n            <div class=\"occurance\">\r\n                <a href=\"\">October 6, 2017</a>\r\n            </div>\r\n            <div class=\"views\">\r\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n            </div>\r\n            <div class=\"sources\">\r\n                <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                <button id=\"source2\"> Source 1</button>\r\n                <button id=\"source3\"> Source 1</button>\r\n                <!-- <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- headline-1 -->\r\n    <div id=\"headline\" class=\"col-sm-3\" style=\"padding-right: 0px;padding-left:0px\">\r\n        <div class=\"overlay\"></div>\r\n        <div id=\"header-image\">\r\n            <img src=\"http://www.placehold.it/315x478\" title=\"\">\r\n            <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n        </div>\r\n        <div class=\"news-part\">\r\n            <div class=\"category red\">\r\n                <a href=\"politics\">Politics</a>\r\n            </div>\r\n            <div class=\"description\">\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n            </div>\r\n            <div class=\"occurance\">\r\n                <a href=\"\">October 6, 2017</a>\r\n            </div>\r\n            <div class=\"views\">\r\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n            </div>\r\n            <div class=\"sources\">\r\n                <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                <button id=\"source2\"> Source 1</button>\r\n                <button id=\"source3\"> Source 1</button>\r\n                <!-- <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- headline-2 -->\r\n    <div id=\"headline\" class=\"col-sm-3\" style=\"padding-right: 0px;padding-left:0px\">\r\n        <div class=\"overlay\"></div>\r\n        <div id=\"header-image\">\r\n            <img src=\"http://www.placehold.it/315x478\" title=\"\">\r\n            <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n        </div>\r\n        <div class=\"news-part\">\r\n            <div class=\"category green\">\r\n                <a href=\"politics\">Politics</a>\r\n            </div>\r\n            <div class=\"description\">\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n            </div>\r\n            <div class=\"occurance\">\r\n                <a href=\"\">October 6, 2017</a>\r\n            </div>\r\n            <div class=\"views\">\r\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n            </div>\r\n            <div class=\"sources\">\r\n                <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                <button id=\"source2\"> Source 1</button>\r\n                <button id=\"source3\"> Source 1</button>\r\n                <!-- <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i> -->\r\n            </div>\r\n        </div>\r\n        <!-- news-part -->\r\n\r\n    </div>\r\n    <!-- headline-3 -->\r\n    <div id=\"headline\" class=\"col-sm-3\" style=\"padding-right: 0px;padding-left:0px\">\r\n        <div class=\"overlay\"></div>\r\n        <div id=\"header-image\">\r\n            <img src=\"http://www.placehold.it/315x478\" title=\"\">\r\n            <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n        </div>\r\n        <div class=\"news-part\">\r\n            <div class=\"category blue\">\r\n                <a href=\"politics\">Politics</a>\r\n            </div>\r\n            <div class=\"description\">\r\n                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n            </div>\r\n            <div class=\"occurance\">\r\n                <a href=\"\">October 6, 2017</a>\r\n            </div>\r\n            <div class=\"views\">\r\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n            </div>\r\n            <div class=\"sources\">\r\n                <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                <button id=\"source2\"> Source 1</button>\r\n                <button id=\"source3\"> Source 1</button>\r\n                <!-- <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i> -->\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- headline-4 -->\r\n</div>\r\n<!-- main-headlines -->\r\n<div class=\"mobile-headline-wrapper\">\r\n    <div class=\"mobile-headline\" style=\"width: 1260px;\">\r\n        <div id=\"headline\">\r\n            <div class=\"overlay\"></div>\r\n            <div id=\"header-image\">\r\n                <img src=\"http://www.placehold.it/315x478\" title=\"\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n            </div>\r\n            <div class=\"news-part\">\r\n                <div class=\"category pink\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <div class=\"occurance\">\r\n                    <a href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <div class=\"views\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <div class=\"sources\">\r\n                    <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                    <button id=\"source2\"> Source 1</button>\r\n                    <button id=\"source3\"> Source 1</button>\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- headline-1 -->\r\n        <div id=\"headline\">\r\n            <div class=\"overlay\"></div>\r\n            <div id=\"header-image\">\r\n                <img src=\"http://www.placehold.it/315x478\" title=\"\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n            </div>\r\n            <div class=\"news-part\">\r\n                <div class=\"category red\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <div class=\"occurance\">\r\n                    <a href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <div class=\"views\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <div class=\"sources\">\r\n                    <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                    <button id=\"source2\"> Source 1</button>\r\n                    <button id=\"source3\"> Source 1</button>\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- headline-2 -->\r\n        <div id=\"headline\">\r\n            <div class=\"overlay\"></div>\r\n            <div id=\"header-image\">\r\n                <img src=\"http://www.placehold.it/315x478\" title=\"\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n            </div>\r\n            <div class=\"news-part\">\r\n                <div class=\"category green\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <div class=\"occurance\">\r\n                    <a href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <div class=\"views\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <div class=\"sources\">\r\n                    <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                    <button id=\"source2\"> Source 1</button>\r\n                    <button id=\"source3\"> Source 1</button>\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- headline-3 -->\r\n        <div id=\"headline\">\r\n            <div class=\"overlay\"></div>\r\n            <div id=\"header-image\">\r\n                <img src=\"http://www.placehold.it/315x478\" title=\"\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n            </div>\r\n            <div class=\"news-part\">\r\n                <div class=\"category blue\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <div class=\"occurance\">\r\n                    <a href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <div class=\"views\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <div class=\"sources\">\r\n                    <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                    <button id=\"source2\"> Source 1</button>\r\n                    <button id=\"source3\"> Source 1</button>\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- headline-4 -->\r\n    </div>\r\n    <!-- mobile-headline-wrapper -->\r\n</div>\r\n<!-- mobile-headline -->\r\n<div id=\"section-two\">\r\n\r\n    <div id=\"news-desk\">\r\n        <h3>NEWS DESK</h3>\r\n        <div class=\"underline\"></div>\r\n        <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\r\n        <div class=\"news1\">\r\n            <div class=\"grid-one\">\r\n                <img src=\"http://www.placehold.it/270x180\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                <div class=\"contents\">\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"sources\">\r\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                        <button id=\"source2\"> Source 1</button>\r\n                        <button id=\"source3\"> Source 1</button>\r\n\r\n                    </div>\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-one -->\r\n            <div class=\"grid-two\">\r\n                <div class=\"category green\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <!-- category green -->\r\n                <h3>This is a news title</h3>\r\n                <div class=\"occurance\">\r\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <!-- occurance -->\r\n                <div class=\"views grey\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <!-- views grey -->\r\n                <div class=\"bookmark\">\r\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\r\n                </div>\r\n                <!-- bookmark -->\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <!-- description -->\r\n            </div>\r\n            <!-- grid-two -->\r\n        </div>\r\n        <!-- news1 -->\r\n        <hr>\r\n        <div class=\"news2\">\r\n            <div class=\"grid-one\">\r\n                <div class=\"category green\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <!-- category green -->\r\n                <h3>This is a news title</h3>\r\n                <div class=\"occurance\">\r\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <!-- occurance -->\r\n                <div class=\"views grey\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <!-- views grey -->\r\n                <div class=\"bookmark\">\r\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\r\n                </div>\r\n                <!-- bookmark -->\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <!-- description -->\r\n            </div>\r\n            <div class=\"grid-two\">\r\n                <img src=\"http://www.placehold.it/270x180\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                <div class=\"contents\">\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"sources\">\r\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                        <button id=\"source2\"> Source 1</button>\r\n                        <button id=\"source3\"> Source 1</button>\r\n\r\n                    </div>\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-two -->\r\n        </div>\r\n        <!-- news2 -->\r\n        <hr>\r\n        <div class=\"news1\">\r\n            <div class=\"grid-one\">\r\n                <img src=\"http://www.placehold.it/270x180\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                <div class=\"contents\">\r\n\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"sources\">\r\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                        <button id=\"source2\"> Source 1</button>\r\n                        <button id=\"source3\"> Source 1</button>\r\n\r\n                    </div>\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-one -->\r\n            <div class=\"grid-two\">\r\n                <div class=\"category green\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <!-- category green -->\r\n                <h3>This is a news title</h3>\r\n                <div class=\"occurance\">\r\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <!-- occurance -->\r\n                <div class=\"views grey\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <!-- views grey -->\r\n                <div class=\"bookmark\">\r\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\r\n                </div>\r\n                <!-- bookmark -->\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <!-- description -->\r\n            </div>\r\n            <!-- grid-two -->\r\n        </div>\r\n        <!-- news1 -->\r\n    </div>\r\n    <!-- news-desk -->\r\n    <div id=\"scandal-watch\">\r\n        <h3>SCANDAL WATCH</h3>\r\n        <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\r\n        <div class=\"underline\"></div>\r\n        <div class=\"grid-one-big\">\r\n            <div class=\"banner-image\">\r\n                <img src=\"http://placehold.it/550x225\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                <div class=\"banner-overlay\"></div>\r\n            </div>\r\n            <div class=\"description\">\r\n                <h3>\"</h3>\r\n                <h3>HISTORIC PRESIDENTIAL CAMPAIGN Scandal</h3>\r\n                <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many presidential\r\n                    campaigns. Some candidates survived. Other didn't</p>\r\n                <div class=\"sources\">\r\n                    <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                    <button id=\"source2\"> Source 1</button>\r\n                    <button id=\"source3\"> Source 1</button>\r\n\r\n                </div>\r\n                <!-- sources -->\r\n                <div class=\"scandal-category\">\r\n                    <div class=\"category green\">\r\n                        <a href=\"politics\">Politics</a>\r\n                    </div>\r\n                    <!-- category green -->\r\n                    <div class=\"occurance\">\r\n                        <p href=\"\">October 6, 2017</p>\r\n                    </div>\r\n                    <!-- occurance -->\r\n                    <div class=\"views\">\r\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                    </div>\r\n                    <!-- views grey -->\r\n                </div>\r\n                <!-- scandal-category -->\r\n            </div>\r\n            <!-- description -->\r\n        </div>\r\n        <!-- grid-one-big -->\r\n        <h3>THINK TANK</h3>\r\n        <div class=\"underline\"></div>\r\n        <div class=\"think-tank\">\r\n            <div class=\"grid-one\">\r\n                <div class=\"banner-image\">\r\n                    <img src=\"http://www.placehold.it/270x180\">\r\n                    <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"lower-buttons\">\r\n                        <div class=\"sources\">\r\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                            <button id=\"source2\"> Source 1</button>\r\n                            <button id=\"source3\"> Source 1</button>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"banner-contents\">\r\n                    <div class=\"category green\">\r\n                        <a href=\"politics\">Politics</a>\r\n                    </div>\r\n                    <!-- category green -->\r\n                    <div class=\"description\">\r\n                        <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many\r\n                            presidential campaigns. Some candidates survived. Other didn't</p>\r\n                    </div>\r\n                    <div class=\"occurance\">\r\n                        <p class=\"grey\" href=\"\">October 6, 2017</p>\r\n                    </div>\r\n                    <!-- occurance -->\r\n                    <div class=\"views grey\">\r\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                    </div>\r\n                    <!-- views grey -->\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-one -->\r\n            <div class=\"grid-one\">\r\n                <div class=\"banner-image\">\r\n                    <img src=\"http://www.placehold.it/270x180\">\r\n                    <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"lower-buttons\">\r\n                        <div class=\"sources\">\r\n                            <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                            <button id=\"source2\"> Source 1</button>\r\n                            <button id=\"source3\"> Source 1</button>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"banner-contents\">\r\n                    <div class=\"category green\">\r\n                        <a href=\"politics\">Politics</a>\r\n                    </div>\r\n                    <!-- category green -->\r\n                    <div class=\"description\">\r\n                        <p>Since the nation's early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didn't.Since the nation's early days, scandals of all kinds have threatened to derail many\r\n                            presidential campaigns. Some candidates survived. Other didn't</p>\r\n                    </div>\r\n                    <div class=\"occurance\">\r\n                        <p class=\"grey\" href=\"\">October 6, 2017</p>\r\n                    </div>\r\n                    <!-- occurance -->\r\n                    <div class=\"views grey\">\r\n                        <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                    </div>\r\n                    <!-- views grey -->\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-one -->\r\n        </div>\r\n        <!-- think-tank -->\r\n    </div>\r\n    <!-- scandal-watch -->\r\n</div>\r\n<!-- section-two -->\r\n<div id=\"around-the-world\">\r\n    <h3>Around the world</h3>\r\n    <div class=\"underline\"></div>\r\n    <button class=\"view-all\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\r\n    <div class=\"atw-wrapper\">\r\n        <div class=\"nextbtn\">\r\n            <button><i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></button>\r\n        </div>\r\n        <div class=\"preButton\">\r\n            <button><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></button>\r\n        </div>\r\n        <div class=\"news1\">\r\n            <div class=\"grid-one\">\r\n                <img src=\"http://www.placehold.it/270x180\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                <div class=\"contents\">\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"sources\">\r\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                        <button id=\"source2\"> Source 1</button>\r\n                        <button id=\"source3\"> Source 1</button>\r\n\r\n                    </div>\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-one -->\r\n            <div class=\"grid-two\">\r\n                <div class=\"category green\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <!-- category green -->\r\n                <h3>This is a news title</h3>\r\n                <div class=\"occurance\">\r\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <!-- occurance -->\r\n                <div class=\"views grey\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <!-- views grey -->\r\n                <div class=\"bookmark\">\r\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\r\n                </div>\r\n                <!-- bookmark -->\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <!-- description -->\r\n            </div>\r\n            <!-- grid-two -->\r\n        </div>\r\n        <!-- news1 -->\r\n        <div class=\"news1\">\r\n            <div class=\"grid-one\">\r\n                <img src=\"http://www.placehold.it/270x180\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                <div class=\"contents\">\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"sources\">\r\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                        <button id=\"source2\"> Source 1</button>\r\n                        <button id=\"source3\"> Source 1</button>\r\n\r\n                    </div>\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-one -->\r\n            <div class=\"grid-two\">\r\n                <div class=\"category green\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <!-- category green -->\r\n                <h3>This is a news title</h3>\r\n                <div class=\"occurance\">\r\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <!-- occurance -->\r\n                <div class=\"views grey\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <!-- views grey -->\r\n                <div class=\"bookmark\">\r\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\r\n                </div>\r\n                <!-- bookmark -->\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <!-- description -->\r\n            </div>\r\n            <!-- grid-two -->\r\n        </div>\r\n        <!-- news1 -->\r\n        <div class=\"news2\">\r\n            <div class=\"grid-one\">\r\n                <img src=\"http://www.placehold.it/270x180\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                <div class=\"contents\">\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"sources\">\r\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                        <button id=\"source2\"> Source 1</button>\r\n                        <button id=\"source3\"> Source 1</button>\r\n\r\n                    </div>\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-one -->\r\n            <div class=\"grid-two\">\r\n                <div class=\"category green\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <!-- category green -->\r\n                <h3>This is a news title</h3>\r\n                <div class=\"occurance\">\r\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <!-- occurance -->\r\n                <div class=\"views grey\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <!-- views grey -->\r\n                <div class=\"bookmark\">\r\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\r\n                </div>\r\n                <!-- bookmark -->\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <!-- description -->\r\n            </div>\r\n            <!-- grid-two -->\r\n        </div>\r\n        <!-- news2 -->\r\n        <div class=\"news2\">\r\n            <div class=\"grid-one\">\r\n                <img src=\"http://www.placehold.it/270x180\">\r\n                <i id=\"playButton\" class=\"fa fa-play-circle playbutton\" aria-hidden=\"true\"></i>\r\n                <div class=\"contents\">\r\n                    <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\r\n                    <div class=\"sources\">\r\n                        <button id=\"source1\" class=\"active\"> Source 1</button>\r\n                        <button id=\"source2\"> Source 1</button>\r\n                        <button id=\"source3\"> Source 1</button>\r\n\r\n                    </div>\r\n                </div>\r\n                <!-- contents -->\r\n            </div>\r\n            <!-- grid-one -->\r\n            <div class=\"grid-two\">\r\n                <div class=\"category green\">\r\n                    <a href=\"politics\">Politics</a>\r\n                </div>\r\n                <!-- category green -->\r\n                <h3>This is a news title</h3>\r\n                <div class=\"occurance\">\r\n                    <a class=\"grey\" href=\"\">October 6, 2017</a>\r\n                </div>\r\n                <!-- occurance -->\r\n                <div class=\"views grey\">\r\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;1465</p>\r\n                </div>\r\n                <!-- views grey -->\r\n                <div class=\"bookmark\">\r\n                    <i class=\"fa fa-bookmark-o\" aria-hidden=\"true\"></i><a href=\"\">&nbsp;READ LATER</a>\r\n                </div>\r\n                <!-- bookmark -->\r\n                <div class=\"description\">\r\n                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n                </div>\r\n                <!-- description -->\r\n            </div>\r\n            <!-- grid-two -->\r\n        </div>\r\n        <!-- news2 -->\r\n    </div>\r\n    <!-- atw-wrapper -->\r\n</div>\r\n<!-- around-the-world -->"

/***/ }),

/***/ "../../../../../src/app/politics-video/politics-video.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoliticsVideoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PoliticsVideoComponent = (function () {
    function PoliticsVideoComponent() {
    }
    PoliticsVideoComponent.prototype.ngOnInit = function () {
    };
    PoliticsVideoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-politics-video',
            template: __webpack_require__("../../../../../src/app/politics-video/politics-video.component.html"),
            styles: [__webpack_require__("../../../../../src/app/politics-video/politics-video.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], PoliticsVideoComponent);
    return PoliticsVideoComponent;
}());



/***/ }),

/***/ "../../../../../src/app/politics/politics.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/politics/politics.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main-headlines -->\n\n\n\n<!-- main-headlines -->\n<app-main-headlines></app-main-headlines>\n<div id=\"section-two\">\n    <!-- news-desk -->\n    <app-news-desk></app-news-desk>\n\n\n    <!-- scandal-watch -->\n    <div id=\"scandal-watch\">\n        <h3>SCANDAL WATCH</h3>\n        <app-scandal-watch></app-scandal-watch>\n        <!-- grid-one-big -->\n        <h3>THINK TANK</h3>\n        <div class=\"underline\"></div>\n        <app-think-tank></app-think-tank>\n\n    </div>\n</div>\n<!-- section-two -->\n<app-around-world></app-around-world>\n<!-- around-the-world -->"

/***/ }),

/***/ "../../../../../src/app/politics/politics.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoliticsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__main_headlines_service__ = __webpack_require__("../../../../../src/app/main-headlines.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PoliticsComponent = (function () {
    function PoliticsComponent(headlinesSercice) {
        this.headlinesSercice = headlinesSercice;
    }
    PoliticsComponent.prototype.ngOnInit = function () {
    };
    PoliticsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-politics',
            template: __webpack_require__("../../../../../src/app/politics/politics.component.html"),
            styles: [__webpack_require__("../../../../../src/app/politics/politics.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__main_headlines_service__["a" /* MainHeadlinesService */]])
    ], PoliticsComponent);
    return PoliticsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/scandal-watch.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScandalWatchService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ScandalWatchService = (function () {
    function ScandalWatchService() {
        this.scandalWatchData = [{
                image: "http://lorempixel.com/550/225",
                title: "HISTORIC PRESIDENTIAL CAMPAIGN SCANDAL",
                description: "Since the nations early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didnt.Since the nations early days, scandals of all kinds have threatened to derail many presidentia campaigns. Some candidates survived. Other didnt",
                sources: "",
                category: "",
                occurance: "October 6, 2017",
                views: "150"
            }];
    }
    ScandalWatchService.prototype.getScandalNews = function () {
        console.log("came here in scandal watch service");
        return this.scandalWatchData;
    };
    ScandalWatchService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ScandalWatchService);
    return ScandalWatchService;
}());



/***/ }),

/***/ "../../../../../src/app/scandal-watch/scandal-watch.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".grid-one-big:hover {\n    cursor: pointer;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/scandal-watch/scandal-watch.component.html":
/***/ (function(module, exports) {

module.exports = "<button class=\"view-all\" routerLink=\"/category-listing\">view more <i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></button>\n<div class=\"underline\"></div>\n<div class=\"grid-one-big\" *ngFor=\"let item of data; let i = index\" routerLink=\"/news-landing\">\n    <div class=\"banner-image\">\n        <img src={{item.image}}>\n        <div class=\"banner-overlay\"></div>\n    </div>\n    <div class=\"description\">\n        <h3>\"</h3>\n        <h3>{{item.title}}</h3>\n        <p>{{item.description}}</p>\n        <div class=\"sources\">\n            <button class=\"active\"> Source 1</button>\n            <button> Source 1</button>\n            <button> Source 1</button>\n        </div>\n        <!-- sources -->\n        <div class=\"scandal-category\">\n            <div class=\"category green\">\n                <a href=\"politics\">Politics</a>\n            </div>\n            <!-- category green -->\n            <div class=\"occurance\">\n                <p href=\"\">{{item.occurance}}</p>\n            </div>\n            <!-- occurance -->\n            <div class=\"views\">\n                <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{item.views}}</p>\n            </div>\n            <!-- views grey -->\n        </div>\n        <!-- scandal-category -->\n    </div>\n    <!-- description -->\n</div>"

/***/ }),

/***/ "../../../../../src/app/scandal-watch/scandal-watch.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScandalWatchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__scandal_watch_service__ = __webpack_require__("../../../../../src/app/scandal-watch.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ScandalWatchComponent = (function () {
    function ScandalWatchComponent(ScandalWatchService) {
        this.ScandalWatchService = ScandalWatchService;
        this.data = '';
    }
    ScandalWatchComponent.prototype.ngOnInit = function () {
        this.data = this.ScandalWatchService.getScandalNews();
        console.log(this.data);
    };
    ScandalWatchComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-scandal-watch',
            template: __webpack_require__("../../../../../src/app/scandal-watch/scandal-watch.component.html"),
            styles: [__webpack_require__("../../../../../src/app/scandal-watch/scandal-watch.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__scandal_watch_service__["a" /* ScandalWatchService */]])
    ], ScandalWatchComponent);
    return ScandalWatchComponent;
}());



/***/ }),

/***/ "../../../../../src/app/scandalwatch-viewdetail/scandalwatch-viewdetail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/scandalwatch-viewdetail/scandalwatch-viewdetail.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/scandalwatch-viewdetail/scandalwatch-viewdetail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScandalwatchViewdetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ScandalwatchViewdetailComponent = (function () {
    function ScandalwatchViewdetailComponent() {
    }
    ScandalwatchViewdetailComponent.prototype.ngOnInit = function () {
    };
    ScandalwatchViewdetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-scandalwatch-viewdetail',
            template: __webpack_require__("../../../../../src/app/scandalwatch-viewdetail/scandalwatch-viewdetail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/scandalwatch-viewdetail/scandalwatch-viewdetail.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ScandalwatchViewdetailComponent);
    return ScandalwatchViewdetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/scrolldemo/scrolldemo.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/scrolldemo/scrolldemo.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n    scrolldemo works!\n</p>\n\n\n\n<div class=\"main-panel\">\n    <div infiniteScroll [infiniteScrollDistance]=\"2\" [infiniteScrollThrottle]=\"50\" [infiniteScrollContainer]=\"selector\" [fromRoot]=\"true\" (scrolled)=\"onScroll()\">\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/scrolldemo/scrolldemo.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScrolldemoComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ScrolldemoComponent = (function () {
    function ScrolldemoComponent() {
    }
    ScrolldemoComponent.prototype.ngOnInit = function () {
    };
    ScrolldemoComponent.prototype.onScroll = function () {
        console.log('scrolled!!');
    };
    ScrolldemoComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-scrolldemo',
            template: __webpack_require__("../../../../../src/app/scrolldemo/scrolldemo.component.html"),
            styles: [__webpack_require__("../../../../../src/app/scrolldemo/scrolldemo.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ScrolldemoComponent);
    return ScrolldemoComponent;
}());



/***/ }),

/***/ "../../../../../src/app/signin-signup/signin-signup.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/signin-signup/signin-signup.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"login-background\"></div>\n<!-- login-background -->\n<div class=\"login-div\">\n    <h3>Join Manotr</h3>\n    <p>Please sign in or create an account to avail our special features</p>\n    <div class=\"social-buttons\" aria-hidden=\"true\">\n        <i class=\"fa fa-facebook fb social-btn\" aria-hidden=\"true\"></i>\n        <i class=\"fa fa-twitter t social-btn\" aria-hidden=\"true\"></i>\n        <i class=\"fa fa-google-plus g social-btn\" aria-hidden=\"true\"></i>\n    </div>\n    <!--  social-buttons -->\n    <p>or via email</p>\n    <div class=\"other-div\">\n        <div>\n            <i class=\"fa fa-user\" aria-hidden=\"true\"></i><input type=\"text\" class=\"name textbox\" name=\"\" placeholder=\"Full Name\">\n        </div>\n        <div>\n            <i>@</i>&nbsp;<input type=\"email\" name=\"\" class=\"email textbox\" placeholder=\"Email\">\n        </div>\n        <div>\n            <i class=\"fa fa-lock\" aria-hidden=\"true\"></i><input type=\"Password\" name=\"\" class=\"textbox password\" placeholder=\"Password\">\n        </div>\n        <div>\n            <button class=\"sign-up\" routerLink=\"/user-dashboard\">Sign Up</button>\n        </div>\n    </div>\n    <!-- other-div -->\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/signin-signup/signin-signup.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SigninSignupComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SigninSignupComponent = (function () {
    function SigninSignupComponent() {
    }
    SigninSignupComponent.prototype.ngOnInit = function () {
    };
    SigninSignupComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-signin-signup',
            template: __webpack_require__("../../../../../src/app/signin-signup/signin-signup.component.html"),
            styles: [__webpack_require__("../../../../../src/app/signin-signup/signin-signup.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SigninSignupComponent);
    return SigninSignupComponent;
}());



/***/ }),

/***/ "../../../../../src/app/simpleslider/simpleslider.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,300,600);", ""]);

// module
exports.push([module.i, "html {\n    border-top: 5px solid #fff;\n    background: #58DDAF;\n    color: #2a2a2a;\n}\n\nhtml,\nbody {\n    margin: 0;\n    padding: 0;\n    font-family: 'Open Sans';\n}\n\nh1 {\n    color: #fff;\n    text-align: center;\n    font-weight: 300;\n}\n\n#slider {\n    position: relative;\n    overflow: hidden;\n    margin: 20px auto 0 auto;\n    border-radius: 4px;\n}\n\n#slider ul {\n    position: relative;\n    margin: 0;\n    padding: 0;\n    height: 200px;\n    list-style: none;\n}\n\n#slider ul li {\n    position: relative;\n    display: block;\n    float: left;\n    margin: 0;\n    padding: 0;\n    width: 500px;\n    height: 300px;\n    background: #ccc;\n    text-align: center;\n    line-height: 300px;\n}\n\na.control_prev,\na.control_next {\n    position: absolute;\n    top: 40%;\n    z-index: 999;\n    display: block;\n    padding: 4% 3%;\n    width: auto;\n    height: auto;\n    background: #2a2a2a;\n    color: #fff;\n    text-decoration: none;\n    font-weight: 600;\n    font-size: 18px;\n    opacity: 0.8;\n    cursor: pointer;\n}\n\na.control_prev:hover,\na.control_next:hover {\n    opacity: 1;\n    -webkit-transition: all 0.2s ease;\n}\n\na.control_prev {\n    border-radius: 0 2px 2px 0;\n}\n\na.control_next {\n    right: 0;\n    border-radius: 2px 0 0 2px;\n}\n\n.slider_option {\n    position: relative;\n    margin: 10px auto;\n    width: 160px;\n    font-size: 18px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/simpleslider/simpleslider.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Incredibly Basic Slider</h1>\n<div id=\"slider\">\n    <a href=\"#\" class=\"control_next\">></a>\n    <a href=\"#\" class=\"control_prev\"></a>\n    <ul>\n        <li>SLIDE 1</li>\n        <li style=\"background: #aaa;\">SLIDE 2</li>\n        <li>SLIDE 3</li>\n        <li style=\"background: #aaa;\">SLIDE 4</li>\n    </ul>\n</div>\n\n<div class=\"slider_option\">\n    <input type=\"checkbox\" id=\"checkbox\">\n    <label for=\"checkbox\">Autoplay Slider</label>\n</div>"

/***/ }),

/***/ "../../../../../src/app/simpleslider/simpleslider.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SimplesliderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SimplesliderComponent = (function () {
    function SimplesliderComponent() {
    }
    SimplesliderComponent.prototype.ngOnInit = function () {
        jQuery(document).ready(function ($) {
            $('#checkbox').change(function () {
                setInterval(function () {
                    moveRight();
                }, 3000);
            });
            var slideCount = $('#slider ul li').length;
            var slideWidth = $('#slider ul li').width();
            var slideHeight = $('#slider ul li').height();
            var sliderUlWidth = slideCount * slideWidth;
            $('#slider').css({
                width: slideWidth,
                height: slideHeight
            });
            $('#slider ul').css({
                width: sliderUlWidth,
                marginLeft: -slideWidth
            });
            $('#slider ul li:last-child').prependTo('#slider ul');
            function moveLeft() {
                $('#slider ul').animate({
                    left: +slideWidth
                }, 200, function () {
                    $('#slider ul li:last-child').prependTo('#slider ul');
                    $('#slider ul').css('left', '');
                });
            }
            ;
            function moveRight() {
                $('#slider ul').animate({
                    left: -slideWidth
                }, 200, function () {
                    $('#slider ul li:first-child').appendTo('#slider ul');
                    $('#slider ul').css('left', '');
                });
            }
            ;
            $('a.control_prev').click(function () {
                moveLeft();
            });
            $('a.control_next').click(function () {
                moveRight();
            });
        });
    };
    SimplesliderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-simpleslider',
            template: __webpack_require__("../../../../../src/app/simpleslider/simpleslider.component.html"),
            styles: [__webpack_require__("../../../../../src/app/simpleslider/simpleslider.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SimplesliderComponent);
    return SimplesliderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/startups/startups.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/startups/startups.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/startups/startups.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartupsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StartupsComponent = (function () {
    function StartupsComponent() {
    }
    StartupsComponent.prototype.ngOnInit = function () {
    };
    StartupsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-startups',
            template: __webpack_require__("../../../../../src/app/startups/startups.component.html"),
            styles: [__webpack_require__("../../../../../src/app/startups/startups.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], StartupsComponent);
    return StartupsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/technology/technology.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/technology/technology.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/technology/technology.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TechnologyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TechnologyComponent = (function () {
    function TechnologyComponent() {
    }
    TechnologyComponent.prototype.ngOnInit = function () {
    };
    TechnologyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-technology',
            template: __webpack_require__("../../../../../src/app/technology/technology.component.html"),
            styles: [__webpack_require__("../../../../../src/app/technology/technology.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TechnologyComponent);
    return TechnologyComponent;
}());



/***/ }),

/***/ "../../../../../src/app/think-tank.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThinkTankService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ThinkTankService = (function () {
    function ThinkTankService() {
        this.thinkTankData = [{
                image: "http://lorempixel.com/550/225",
                description: "Since the nations early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didnt.Since the nations early days, scandals of all kinds have threatened to derail many presidentia campaigns. Some candidates survived. Other didnt",
                sources: "",
                category: "",
                occurance: "October 6, 2017",
                views: "150"
            },
            {
                image: "http://lorempixel.com/550/225",
                description: "Since the nations early days, scandals of all kinds have threatened to derail many presidential campaigns. Some candidates survived. Other didnt.Since the nations early days, scandals of all kinds have threatened to derail many presidentia campaigns. Some candidates survived. Other didnt",
                sources: "",
                category: "",
                occurance: "October 6, 2017",
                views: "150"
            }];
    }
    ThinkTankService.prototype.getThinkTankNews = function () {
        console.log("inside think tank news");
        return this.thinkTankData;
    };
    ThinkTankService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ThinkTankService);
    return ThinkTankService;
}());



/***/ }),

/***/ "../../../../../src/app/think-tank/think-tank.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".grid-one:hover {\n    cursor: pointer;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/think-tank/think-tank.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"think-tank\">\n    <div class=\"row\">\n        <div class=\"grid-one\" *ngFor=\"let item of data; let i = index\" routerLink=\"/news-landing\">\n            <div class=\"banner-image\">\n                <img src=\"{{item.image}}\">\n                <button class=\"menu\"><i class=\"fa fa-ellipsis-v\" aria-hidden=\"true\"></i></button>\n                <div class=\"lower-buttons\">\n                    <div class=\"sources\">\n                        <button class=\"active\"> Source 1</button>\n                        <button> Source 1</button>\n                        <button> Source 1</button>\n                    </div>\n                </div>\n            </div>\n            <div class=\"banner-contents\">\n                <div class=\"category green\">\n                    <a href=\"politics\">Politics</a>\n                </div>\n                <!-- category green -->\n                <div class=\"description\">\n                    <p>{{item.description}}</p>\n                </div>\n                <div class=\"occurance\">\n                    <p class=\"grey\" href=\"\">{{item.occurance}}</p>\n                </div>\n                <!-- occurance -->\n                <div class=\"views grey\">\n                    <p><i class=\"fa fa-eye\" aria-hidden=\"true\"></i>&nbsp;{{item.views}}</p>\n                </div>\n                <!-- views grey -->\n            </div>\n            <!-- contents -->\n        </div>\n        <!-- grid-one -->\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/think-tank/think-tank.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThinkTankComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__think_tank_service__ = __webpack_require__("../../../../../src/app/think-tank.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ThinkTankComponent = (function () {
    function ThinkTankComponent(ThinkTankService) {
        this.ThinkTankService = ThinkTankService;
        this.data = '';
    }
    ThinkTankComponent.prototype.ngOnInit = function () {
        this.data = this.ThinkTankService.getThinkTankNews();
        console.log(this.data);
    };
    ThinkTankComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-think-tank',
            template: __webpack_require__("../../../../../src/app/think-tank/think-tank.component.html"),
            styles: [__webpack_require__("../../../../../src/app/think-tank/think-tank.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__think_tank_service__["a" /* ThinkTankService */]])
    ], ThinkTankComponent);
    return ThinkTankComponent;
}());



/***/ }),

/***/ "../../../../../src/app/tv-shows/tv-shows.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/tv-shows/tv-shows.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/tv-shows/tv-shows.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TvShowsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TvShowsComponent = (function () {
    function TvShowsComponent() {
    }
    TvShowsComponent.prototype.ngOnInit = function () {
    };
    TvShowsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-tv-shows',
            template: __webpack_require__("../../../../../src/app/tv-shows/tv-shows.component.html"),
            styles: [__webpack_require__("../../../../../src/app/tv-shows/tv-shows.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TvShowsComponent);
    return TvShowsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/user-dashboard/user-dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/user-dashboard/user-dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  user-dashboard works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/user-dashboard/user-dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserDashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserDashboardComponent = (function () {
    function UserDashboardComponent() {
    }
    UserDashboardComponent.prototype.ngOnInit = function () {
    };
    UserDashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-user-dashboard',
            template: __webpack_require__("../../../../../src/app/user-dashboard/user-dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/user-dashboard/user-dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UserDashboardComponent);
    return UserDashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/videos/videos.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/videos/videos.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/videos/videos.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VideosComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VideosComponent = (function () {
    function VideosComponent() {
    }
    VideosComponent.prototype.ngOnInit = function () {
    };
    VideosComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-videos',
            template: __webpack_require__("../../../../../src/app/videos/videos.component.html"),
            styles: [__webpack_require__("../../../../../src/app/videos/videos.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], VideosComponent);
    return VideosComponent;
}());



/***/ }),

/***/ "../../../../../src/app/viewmore.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewmoreService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ViewmoreService = (function () {
    function ViewmoreService() {
        this.news = [{
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }, {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }, {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }, {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            },
            {
                image: "http://www.placehold.it/270x180",
                sources: ["source1", "source2", "source3"],
                title: "This is a news title",
                occurance: "October 6, 2017",
                views: "1465",
                description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }];
    }
    ViewmoreService.prototype.getmoreNews = function () {
        console.log("inide view more");
        return this.news;
    };
    ViewmoreService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ViewmoreService);
    return ViewmoreService;
}());



/***/ }),

/***/ "../../../../../src/app/world-news/world-news.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/world-news/world-news.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"height:500px\">\n    <img src=\"../../assets/images/wip.gif\" alt=\"work is in progress\" style=\"height: 250px; width:400px;margin-left:450px;margin-bottom:50px;\">\n</div>"

/***/ }),

/***/ "../../../../../src/app/world-news/world-news.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WorldNewsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WorldNewsComponent = (function () {
    function WorldNewsComponent() {
    }
    WorldNewsComponent.prototype.ngOnInit = function () {
    };
    WorldNewsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-world-news',
            template: __webpack_require__("../../../../../src/app/world-news/world-news.component.html"),
            styles: [__webpack_require__("../../../../../src/app/world-news/world-news.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], WorldNewsComponent);
    return WorldNewsComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map