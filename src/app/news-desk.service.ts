import { Injectable } from '@angular/core';

@Injectable()
export class NewsDeskService {

  newsDeskData: any = [
    {
      image: "http://www.lorempixel.com/315/478",
      title: "demo news",
      description: "Patrick Bordier, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
      occurance: "Octombor 6,2017",
      views: "85",
      sources: "demo"
    },
    {
      image: "http://www.lorempixel.com/315/478",
      title: "demo news",
      description: "Patrick Bordier, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
      occurance: "Octombor 6,2017",
      views: "85",
      sources: "demo"
    },
    {
      image: "http://www.lorempixel.com/315/478",
      title: "demo news",
      description: "Patrick Bordier, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
      occurance: "Octombor 6,2017",
      views: "85",
      sources: "demo"
    }];

  constructor() { }
  getNewsDews() {
    console.log("come here in news desk servie");
    return this.newsDeskData;
  }

}
