import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsdeskViewdetailComponent } from './newsdesk-viewdetail.component';

describe('NewsdeskViewdetailComponent', () => {
  let component: NewsdeskViewdetailComponent;
  let fixture: ComponentFixture<NewsdeskViewdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsdeskViewdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsdeskViewdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
