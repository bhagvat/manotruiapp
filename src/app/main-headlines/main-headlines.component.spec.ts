import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainHeadlinesComponent } from './main-headlines.component';

describe('MainHeadlinesComponent', () => {
  let component: MainHeadlinesComponent;
  let fixture: ComponentFixture<MainHeadlinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainHeadlinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainHeadlinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
