import { Component, OnInit } from '@angular/core';
import { MainHeadlinesService } from '../main-headlines.service'
declare var $: any;

@Component({
  selector: 'app-main-headlines',
  templateUrl: './main-headlines.component.html',
  styleUrls: ['./main-headlines.component.css']
})
export class MainHeadlinesComponent implements OnInit {

  data: any = '';
  activeBtn: any = 0;
  timer:any='';
  constructor(private headlinesSercice: MainHeadlinesService) { }


  ngOnInit() {
    this.data = this.headlinesSercice.getHeadlines();
    console.log(this.data);

    }
  
  
}
