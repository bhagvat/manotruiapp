import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsLandingTagsComponent } from './news-landing-tags.component';

describe('NewsLandingTagsComponent', () => {
  let component: NewsLandingTagsComponent;
  let fixture: ComponentFixture<NewsLandingTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsLandingTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsLandingTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
