import { Component, OnInit } from '@angular/core';
import {NewslandingNewsDetailsService} from '../newslanding-news-details.service';

@Component({
  selector: 'app-news-landing-news-details',
  templateUrl: './news-landing-news-details.component.html',
  styleUrls: ['./news-landing-news-details.component.css']
})
export class NewsLandingNewsDetailsComponent implements OnInit {

  data:any='';

  constructor( private newsdetails:NewslandingNewsDetailsService ) { }

  ngOnInit() {
  this.data = this.newsdetails.getNewsDetails();
  console.log("data"+this.data)
  }
  
}
