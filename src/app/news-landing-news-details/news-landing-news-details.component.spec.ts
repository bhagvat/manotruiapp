import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsLandingNewsDetailsComponent } from './news-landing-news-details.component';

describe('NewsLandingNewsDetailsComponent', () => {
  let component: NewsLandingNewsDetailsComponent;
  let fixture: ComponentFixture<NewsLandingNewsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsLandingNewsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsLandingNewsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
