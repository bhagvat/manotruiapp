import { TestBed, inject } from '@angular/core/testing';

import { NewslandingBottomdivnewsService } from './newslanding-bottomdivnews.service';

describe('NewslandingBottomdivnewsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewslandingBottomdivnewsService]
    });
  });

  it('should be created', inject([NewslandingBottomdivnewsService], (service: NewslandingBottomdivnewsService) => {
    expect(service).toBeTruthy();
  }));
});
