import { Component, OnInit } from '@angular/core';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import {AroundWorldService} from '../around-world.service'

@Component({
  selector: 'app-coroseldemo',
  templateUrl: './coroseldemo.component.html',
  styleUrls: ['./coroseldemo.component.css']
})
export class CoroseldemoComponent implements OnInit {
items: Array<any> = []
data:Array<any>=[]

  constructor(private AroundWorldService:AroundWorldService) {
    this.items = [
      { name: '../../assets/images/manotr-logo.gif' },
      { name: '../../assets/images/manotr-logo.png' },
      { name: '../../assets/images/manotr-logo.gif' },
      { name: '../../assets/images/manotr-logo.png' },
       { name: '../../assets/images/manotr-logo.gif' },
      { name: '../../assets/images/manotr-logo.png' },
      { name: '../../assets/images/manotr-logo.gif' },
      { name: '../../assets/images/manotr-logo.png' },
      { name: '../../assets/images/manotr-logo.gif' },
      { name: '../../assets/images/manotr-logo.png' },
      { name: '../../assets/images/manotr-logo.gif' },
      { name: '../../assets/images/manotr-logo.png' },
    ];
     
     this.data =  this.AroundWorldService.getAroundWordNews() 

   }

  ngOnInit() {
  }

}
