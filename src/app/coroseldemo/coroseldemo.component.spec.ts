import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoroseldemoComponent } from './coroseldemo.component';

describe('CoroseldemoComponent', () => {
  let component: CoroseldemoComponent;
  let fixture: ComponentFixture<CoroseldemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoroseldemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoroseldemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
