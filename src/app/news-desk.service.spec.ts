import { TestBed, inject } from '@angular/core/testing';

import { NewsDeskService } from './news-desk.service';

describe('NewsDeskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewsDeskService]
    });
  });

  it('should be created', inject([NewsDeskService], (service: NewsDeskService) => {
    expect(service).toBeTruthy();
  }));
});
