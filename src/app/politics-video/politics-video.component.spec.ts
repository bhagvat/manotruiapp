import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoliticsVideoComponent } from './politics-video.component';

describe('PoliticsVideoComponent', () => {
  let component: PoliticsVideoComponent;
  let fixture: ComponentFixture<PoliticsVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoliticsVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoliticsVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
