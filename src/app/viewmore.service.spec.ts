import { TestBed, inject } from '@angular/core/testing';

import { ViewmoreService } from './viewmore.service';

describe('ViewmoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewmoreService]
    });
  });

  it('should be created', inject([ViewmoreService], (service: ViewmoreService) => {
    expect(service).toBeTruthy();
  }));
});
