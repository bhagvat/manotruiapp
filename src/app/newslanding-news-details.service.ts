import { Injectable } from '@angular/core';

@Injectable()
export class NewslandingNewsDetailsService {

newsDetails=
{
    sources: [
      "source1",
      "source2",
      "source3"],
    title: "Trump says minorities \'want\' and \'need\' more police protection than other Americans",
    occurance: "October 6, 2017",
    description: "President Trump said Wednesday that minorities \“want\” and “need” more police protection than other Americans, and blamed Democrats for a \“crazy\” number of murders in Chicago and other large cities.In an interview with Sean Hannity, the Fox News Channel anchor who also is a friend and informal adviser to the president, Trump said police in big cities are \“not allowed\” to respond to what he described as rampant crime because \“they have to be politically correct.\”",
    image: "http://placehold.it/300x200",
    redMoreLink: "http://zeenews.india.com/india/gdp-means-gross-divisive-politics-for-narendra-modi-rahul-gandhi-2072232.html",
    tags: [
      "tags1", "tags1",
      "tags1",
      "tags1",
      "tags1",
      "tags1"
    ]
  }

  constructor() { }


getNewsDetails() {
  console.log("inside news details");
    return this.newsDetails;
  }
}
