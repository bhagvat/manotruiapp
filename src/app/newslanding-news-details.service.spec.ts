import { TestBed, inject } from '@angular/core/testing';

import { NewslandingNewsDetailsService } from './newslanding-news-details.service';

describe('NewslandingNewsDetailsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewslandingNewsDetailsService]
    });
  });

  it('should be created', inject([NewslandingNewsDetailsService], (service: NewslandingNewsDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
