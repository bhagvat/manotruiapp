import { Component, OnInit } from '@angular/core';
import { NewsLandingRelatedNewsRightService } from '../news-landing-related-news-right.service'

@Component({
  selector: 'app-news-landing-related-news-rightt-div',
  templateUrl: './news-landing-related-news-rightt-div.component.html',
  styleUrls: ['./news-landing-related-news-rightt-div.component.css']
})
export class NewsLandingRelatedNewsRighttDivComponent implements OnInit {
data:any='';
  constructor(private  relatedNews : NewsLandingRelatedNewsRightService) { }

  ngOnInit() {
this.data = this.relatedNews.getRelatedNews();
console.log(this.data)
  }

}
