import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsLandingRelatedNewsRighttDivComponent } from './news-landing-related-news-rightt-div.component';

describe('NewsLandingRelatedNewsRighttDivComponent', () => {
  let component: NewsLandingRelatedNewsRighttDivComponent;
  let fixture: ComponentFixture<NewsLandingRelatedNewsRighttDivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsLandingRelatedNewsRighttDivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsLandingRelatedNewsRighttDivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
