import { Component, OnInit } from '@angular/core';
import { ViewmoreService } from '../viewmore.service'

@Component({
  selector: 'app-category-listing',
  templateUrl: './category-listing.component.html',
  styleUrls: ['./category-listing.component.css']
})
export class CategoryListingComponent implements OnInit {
  data: any = '';
  posts: any = '';
  i: any = '';
  myArray: any = '';
  chunk_size: any = '';
  result: any = '';
  datas: any = [];
  res: any = '';
  count: number = 0;
  constructor(private viewmore: ViewmoreService) { }

  ngOnInit() {

    this.data = this.viewmore.getmoreNews();
    // myArray = this.data;
    // chunk_size = 10;

    this.res = this.chunkArray(this.data, 2);

    for (let i = 0; i < this.res[this.count].length; i++) {
      this.res[this.count][i].cardNo = this.count + '-' + i;
      this.datas.push(this.res[this.count][i]);
    }
    this.count++;

  }



  chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
      let myChunk = myArray.slice(index, index + chunk_size);
      // Do something if you want with the group
      tempArray.push(myChunk);
    }

    console.log(tempArray);
    return tempArray;
  }




  onScroll() {
    let len = this.res.length;
    if (this.count < len) {

      for (let i = 0; i < this.res[this.count].length; i++) {
        this.res[this.count][i].cardNo = this.count + '-' + i;
        this.datas.push(this.res[this.count][i]);
      }
      this.count++;
    }


    // this.datas.push(this.res[this.count])
    //this.datas=this.res[this.count]
    console.log('scrolled!!')

  }
}
