import { TestBed, inject } from '@angular/core/testing';

import { ScandalWatchService } from './scandal-watch.service';

describe('ScandalWatchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScandalWatchService]
    });
  });

  it('should be created', inject([ScandalWatchService], (service: ScandalWatchService) => {
    expect(service).toBeTruthy();
  }));
});
