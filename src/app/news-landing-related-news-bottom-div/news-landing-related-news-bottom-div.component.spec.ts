import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsLandingRelatedNewsBottomDivComponent } from './news-landing-related-news-bottom-div.component';

describe('NewsLandingRelatedNewsBottomDivComponent', () => {
  let component: NewsLandingRelatedNewsBottomDivComponent;
  let fixture: ComponentFixture<NewsLandingRelatedNewsBottomDivComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsLandingRelatedNewsBottomDivComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsLandingRelatedNewsBottomDivComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
