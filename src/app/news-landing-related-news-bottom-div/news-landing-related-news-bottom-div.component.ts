import { Component, OnInit } from '@angular/core';
import {NewslandingBottomdivnewsService} from '../newslanding-bottomdivnews.service'
@Component({
  selector: 'app-news-landing-related-news-bottom-div',
  templateUrl: './news-landing-related-news-bottom-div.component.html',
  styleUrls: ['./news-landing-related-news-bottom-div.component.css']
})
export class NewsLandingRelatedNewsBottomDivComponent implements OnInit {
data:any='';
  constructor(private relatedNews:NewslandingBottomdivnewsService) { }

  ngOnInit() {
   
this.data=this.relatedNews.getRelatedNews();
 console.log(this.data)  
}

}
