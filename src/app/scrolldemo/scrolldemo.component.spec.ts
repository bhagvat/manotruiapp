import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrolldemoComponent } from './scrolldemo.component';

describe('ScrolldemoComponent', () => {
  let component: ScrolldemoComponent;
  let fixture: ComponentFixture<ScrolldemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrolldemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrolldemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
