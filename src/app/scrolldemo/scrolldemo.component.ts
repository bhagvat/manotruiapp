import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scrolldemo',
  templateUrl: './scrolldemo.component.html',
  styleUrls: ['./scrolldemo.component.css']
})
export class ScrolldemoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onScroll(){
    console.log('scrolled!!')

  }

}
