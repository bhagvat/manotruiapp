import { Component, OnInit } from '@angular/core';
import { ThinkTankService } from '../think-tank.service'

@Component({
  selector: 'app-think-tank',
  templateUrl: './think-tank.component.html',
  styleUrls: ['./think-tank.component.css']
})
export class ThinkTankComponent implements OnInit {
data :any ='';
  constructor(private ThinkTankService : ThinkTankService) { }

  ngOnInit() {
    this.data=  this.ThinkTankService.getThinkTankNews();
    console.log(this.data);
  }

}
