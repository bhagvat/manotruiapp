import { TestBed, inject } from '@angular/core/testing';

import { MainHeadlinesService } from './main-headlines.service';

describe('MainHeadlinesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MainHeadlinesService]
    });
  });

  it('should be created', inject([MainHeadlinesService], (service: MainHeadlinesService) => {
    expect(service).toBeTruthy();
  }));
});
