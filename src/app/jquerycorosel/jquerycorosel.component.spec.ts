import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JquerycoroselComponent } from './jquerycorosel.component';

describe('JquerycoroselComponent', () => {
  let component: JquerycoroselComponent;
  let fixture: ComponentFixture<JquerycoroselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JquerycoroselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JquerycoroselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
