import { Component, OnInit } from '@angular/core';
import { NewsDeskService } from '../news-desk.service'

@Component({
  selector: 'app-news-desk',
  templateUrl: './news-desk.component.html',
  styleUrls: ['./news-desk.component.css']
})
export class NewsDeskComponent implements OnInit {
  data:any='';

  constructor(private NewsDeskService: NewsDeskService) { }

  ngOnInit() {

    this.data = this.NewsDeskService.getNewsDews();
    console.log(this.data);
  }

}
