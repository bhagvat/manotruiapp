import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDeskComponent } from './news-desk.component';

describe('NewsDeskComponent', () => {
  let component: NewsDeskComponent;
  let fixture: ComponentFixture<NewsDeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsDeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
