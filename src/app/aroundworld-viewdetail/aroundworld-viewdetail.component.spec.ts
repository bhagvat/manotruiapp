import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AroundworldViewdetailComponent } from './aroundworld-viewdetail.component';

describe('AroundworldViewdetailComponent', () => {
  let component: AroundworldViewdetailComponent;
  let fixture: ComponentFixture<AroundworldViewdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AroundworldViewdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AroundworldViewdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
