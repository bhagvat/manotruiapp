import { Injectable } from '@angular/core';

@Injectable()
export class MainHeadlinesService {
  mainHeadlines: any = [
    {
     
      sources:[
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 1, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news 1",
          showdate: "Octombor 6,2017",
          views: "5",
        },
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 3, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news 2",
          showdate: "Octombor 7,2017",
          views: "80",
        },
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 3, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news 3",
          showdate: "Octombor 8,2017",
          views: "75",
        }
      ]
    },
    {
      
      sources:[
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 11, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news",
          showdate: "Octombor 6,2017",
          views: "95",
        },
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 12, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news",
          showdate: "Octombor 7,2017",
          views: "85",
        },
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 13, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news",
          showdate: "Octombor 8,2017",
          views: "15",
        }
      ]
    },
    {
      
      sources:[
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 21, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news",
          showdate: "Octombor 6,2017",
          views: "85",
        },
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 21, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news",
          showdate: "Octombor 7,2017",
          views: "85",
        },
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 23, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news",
          showdate: "Octombor 8,2017",
          views: "85",
        }
      ]
    },
    {
      
      sources:[
        {
          image: "http://www.lorempixel.com/315/478",
          description :"Patrick Bordier 31, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
          title: "demo news",
          showdate: "Octombor 6,2017",
          views: "85",
        },
        {
          image: "http://www.lorempixel.com/315/478",
      description :"Patrick Bordier 32, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
      title: "demo news",
      showdate: "Octombor 7,2017",
      views: "85",
        },
        {
          image: "http://www.lorempixel.com/315/478",
      description :"Patrick Bordier 33, physiotherapist with the French national tennis team since 2014, collapsed and died in Rio on Friday, the president of France's Olympic Committee (CNOSF) said.",
      title: "demo news",
      showdate: "Octombor 8,2017",
      views: "85",
        }
      ]
    }];

  constructor() { }

  getHeadlines() {
    console.log("come here in news desk servie");
    return this.mainHeadlines;
  }
}
