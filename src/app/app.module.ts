/* import requird global module for all components here */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';


/* import all components here */
import { AppComponent } from './app.component';
import { MainHeadlinesComponent } from './main-headlines/main-headlines.component';
import { NewsDeskComponent } from './news-desk/news-desk.component';
import { ScandalWatchComponent } from './scandal-watch/scandal-watch.component';
import { ThinkTankComponent } from './think-tank/think-tank.component';
import { AroundWorldComponent } from './around-world/around-world.component';
import { WorldNewsComponent } from './world-news/world-news.component';
import { CricketComponent } from './cricket/cricket.component';
import { TechnologyComponent } from './technology/technology.component';
import { StartupsComponent } from './startups/startups.component';
import { GamingComponent } from './gaming/gaming.component';
import { TvShowsComponent } from './tv-shows/tv-shows.component';
import { BollywoodComponent } from './bollywood/bollywood.component';
import { FinanceComponent } from './finance/finance.component';
import { AddnewComponent } from './addnew/addnew.component';
import { PoliticsComponent } from './politics/politics.component';
import { VideosComponent } from './videos/videos.component';
import { ScandalwatchViewdetailComponent } from './scandalwatch-viewdetail/scandalwatch-viewdetail.component';
import { NewsdeskViewdetailComponent } from './newsdesk-viewdetail/newsdesk-viewdetail.component';
import { AroundworldViewdetailComponent } from './aroundworld-viewdetail/aroundworld-viewdetail.component';
import { LoginSignupComponent } from './login-signup/login-signup.component';
import { SigninSignupComponent } from './signin-signup/signin-signup.component';
import { NewsLandingComponent } from './news-landing/news-landing.component';
import { NewsListComponent } from './news-list/news-list.component';
import { CategoryListingComponent } from './category-listing/category-listing.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { PoliticsVideoComponent } from './politics-video/politics-video.component';
import {CoroseldemoComponent } from './coroseldemo/coroseldemo.component'
import { JquerycoroselComponent } from './jquerycorosel/jquerycorosel.component';
import { SimplesliderComponent } from './simpleslider/simpleslider.component';
import { NewsLandingNewsDetailsComponent } from './news-landing-news-details/news-landing-news-details.component';
import { NewsLandingRelatedNewsRighttDivComponent } from './news-landing-related-news-rightt-div/news-landing-related-news-rightt-div.component';
import { NewsLandingGraphComponent } from './news-landing-graph/news-landing-graph.component';
import { NewsLandingRelatedNewsBottomDivComponent } from './news-landing-related-news-bottom-div/news-landing-related-news-bottom-div.component';
import { NewsLandingTagsComponent } from './news-landing-tags/news-landing-tags.component'




/* import all services here */

import { MainHeadlinesService } from './main-headlines.service';
import { ScandalWatchService } from './scandal-watch.service';
import { ThinkTankService } from './think-tank.service';
import { AroundWorldService } from './around-world.service';
import { NewsDeskService } from './news-desk.service';
import { NewsLandingRelatedNewsRightService } from './news-landing-related-news-right.service'
import {NewslandingNewsDetailsService} from './newslanding-news-details.service'
import {NewslandingBottomdivnewsService } from  './newslanding-bottomdivnews.service';
import { ScrolldemoComponent } from './scrolldemo/scrolldemo.component';
import {ViewmoreService} from './viewmore.service';
import { CategoryMainComponent } from './category-main/category-main.component'


/* add routing here */
const appRoutes: Routes = [{
  path: '',
  component: PoliticsComponent,
},
{
  path: 'worldnews',
  component: WorldNewsComponent,
},
{
  path: 'cricket',
  component: CricketComponent,
},
{
  path: 'technology',
  component: TechnologyComponent,
},
{
  path: 'startup',
  component: StartupsComponent,
},
{
  path: 'gaming',
  component: GamingComponent,
},
{
  path: 'tvshows',
  component: TvShowsComponent,
},
{
  path: 'bollywood',
  component: BollywoodComponent,
},
{
  path: 'finance',
  component: FinanceComponent,
},
{
  path: 'videos',
  component: VideosComponent,
},
{
  path: 'aroundworld-details',
  component: AroundworldViewdetailComponent
},

{
  path: 'newsdesk-details',
  component: NewsdeskViewdetailComponent
},
{
  path: 'scandalwatch-details',
  component: ScandalwatchViewdetailComponent
},
{
  path: 'signin-signup',
  component: SigninSignupComponent
},
{
  path: 'news-landing',
  component: NewsLandingComponent
},
{
  path: 'news-list',
  component: NewsListComponent
},
{
  path: 'category-listing',
  component: CategoryListingComponent
},
{
  path: 'user-dahboard',
  component: UserDashboardComponent
},
{
  path: 'politics-video',
  component: PoliticsVideoComponent
},
{
  path:'corosel',
  component:CoroseldemoComponent
},
{
  path:'jqcorosel',
  component:JquerycoroselComponent
},
{
  path:'simpleslider',
  component:SimplesliderComponent
},
{
  path:'scrolldemo',
  component:ScrolldemoComponent
},
{
  path:'maincategory',
  component:CategoryMainComponent
}

]



/* add all compoenets reference here */
@NgModule({
  declarations: [
    AppComponent,
    MainHeadlinesComponent,
    NewsDeskComponent,
    ScandalWatchComponent,
    ThinkTankComponent,
    AroundWorldComponent,
    WorldNewsComponent,
    CricketComponent,
    TechnologyComponent,
    StartupsComponent,
    GamingComponent,
    TvShowsComponent,
    BollywoodComponent,
    FinanceComponent,
    AddnewComponent,
    PoliticsComponent,
    VideosComponent,
    ScandalwatchViewdetailComponent,
    NewsdeskViewdetailComponent,
    AroundworldViewdetailComponent,
    LoginSignupComponent,
    SigninSignupComponent,
    NewsLandingComponent,
    NewsListComponent,
    CategoryListingComponent,
    UserDashboardComponent,
    PoliticsVideoComponent,
    CoroseldemoComponent,
    JquerycoroselComponent,
    SimplesliderComponent,
    NewsLandingNewsDetailsComponent,
    NewsLandingRelatedNewsRighttDivComponent,
    NewsLandingGraphComponent,
    NewsLandingRelatedNewsBottomDivComponent,
    NewsLandingTagsComponent,
    ScrolldemoComponent,
    CategoryMainComponent
  ],

  /*add all import module refeence here */
  imports: [
    BrowserModule,
    InfiniteScrollModule,
    RouterModule,
    Ng2CarouselamosModule,
    RouterModule.forRoot(
      appRoutes
    )

  ],

  /* add all of yours services reference here inside providers array */
  providers: [ViewmoreService,NewslandingBottomdivnewsService,MainHeadlinesService,NewsLandingRelatedNewsRightService,NewslandingNewsDetailsService, ScandalWatchService, ThinkTankService, AroundWorldService, NewsDeskService],

  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);