import { TestBed, inject } from '@angular/core/testing';

import { AroundWorldService } from './around-world.service';

describe('AroundWorldService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AroundWorldService]
    });
  });

  it('should be created', inject([AroundWorldService], (service: AroundWorldService) => {
    expect(service).toBeTruthy();
  }));
});
