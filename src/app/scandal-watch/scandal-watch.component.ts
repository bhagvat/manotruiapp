import { Component, OnInit } from '@angular/core';
import { ScandalWatchService } from '../scandal-watch.service'

@Component({
  selector: 'app-scandal-watch',
  templateUrl: './scandal-watch.component.html',
  styleUrls: ['./scandal-watch.component.css']
})
export class ScandalWatchComponent implements OnInit {
data :any='';
  constructor(private ScandalWatchService:ScandalWatchService) { }

  ngOnInit() {
    this.data = this.ScandalWatchService.getScandalNews();
   console.log(this.data);
  }

}
