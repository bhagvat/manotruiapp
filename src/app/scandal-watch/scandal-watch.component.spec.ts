import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScandalWatchComponent } from './scandal-watch.component';

describe('ScandalWatchComponent', () => {
  let component: ScandalWatchComponent;
  let fixture: ComponentFixture<ScandalWatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScandalWatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScandalWatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
