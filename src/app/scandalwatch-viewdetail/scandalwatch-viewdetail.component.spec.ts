import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScandalwatchViewdetailComponent } from './scandalwatch-viewdetail.component';

describe('ScandalwatchViewdetailComponent', () => {
  let component: ScandalwatchViewdetailComponent;
  let fixture: ComponentFixture<ScandalwatchViewdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScandalwatchViewdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScandalwatchViewdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
