import { TestBed, inject } from '@angular/core/testing';

import { NewsLandingRelatedNewsRightService } from './news-landing-related-news-right.service';

describe('NewsLandingRelatedNewsRightService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewsLandingRelatedNewsRightService]
    });
  });

  it('should be created', inject([NewsLandingRelatedNewsRightService], (service: NewsLandingRelatedNewsRightService) => {
    expect(service).toBeTruthy();
  }));
});
