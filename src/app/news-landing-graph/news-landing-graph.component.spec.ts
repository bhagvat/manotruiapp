import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsLandingGraphComponent } from './news-landing-graph.component';

describe('NewsLandingGraphComponent', () => {
  let component: NewsLandingGraphComponent;
  let fixture: ComponentFixture<NewsLandingGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsLandingGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsLandingGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
