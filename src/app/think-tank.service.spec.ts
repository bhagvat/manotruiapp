import { TestBed, inject } from '@angular/core/testing';

import { ThinkTankService } from './think-tank.service';

describe('ThinkTankService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThinkTankService]
    });
  });

  it('should be created', inject([ThinkTankService], (service: ThinkTankService) => {
    expect(service).toBeTruthy();
  }));
});
