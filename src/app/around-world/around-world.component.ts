import { Component, OnInit } from '@angular/core';
import {AroundWorldService} from '../around-world.service'

@Component({
  selector: 'app-around-world',
  templateUrl: './around-world.component.html',
  styleUrls: ['./around-world.component.css']
})

export class AroundWorldComponent implements OnInit {
data:any='';
items:any='';

  constructor(private AroundWorldService : AroundWorldService) { }

  ngOnInit() {
   
  this.data =  this.AroundWorldService.getAroundWordNews()
  console.log(this.data)
  }

}
