import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AroundWorldComponent } from './around-world.component';

describe('AroundWorldComponent', () => {
  let component: AroundWorldComponent;
  let fixture: ComponentFixture<AroundWorldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AroundWorldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AroundWorldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
